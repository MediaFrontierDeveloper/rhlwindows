﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Drawing;
using System.Windows.Forms;
using Newtonsoft.Json;
using RHL.lib.Media;

namespace RHL.lib.Schema
{
    [Serializable()]
    public class Node
    {
        [JsonProperty("nid")]
        public int Nid { get; set; }

        [JsonProperty("tid")]
        public int Tid { get; set; }

        [JsonProperty("langCode")]
        public string LangCode { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("published")]
        public int Published { get; set; }

        [JsonProperty("updated")]
        public int Updated { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("image_data")]
        public string ImageData { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        public Image ThumbNail
        {
            get
            {
                if(!string.IsNullOrEmpty(ImageData))
                {
                    return ImageManager.Base64ToImage(ImageData);
                }
                else
                {
                    return null;
                }
            }
        }

        public string DisplayDate
        {
            get
            {
                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                return dtDateTime.AddSeconds(this.Published).ToLocalTime().ToString("dd MMMM yyyy");
            }
        }
    }
}
