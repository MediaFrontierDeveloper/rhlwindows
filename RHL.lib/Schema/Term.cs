﻿using Newtonsoft.Json;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using RHL.lib.Media;

namespace RHL.lib.Schema
{
    [Serializable()]
    public class Term
    {
        [JsonProperty("tid")]
        public Int32 Tid { get; set; }

        [JsonProperty("parent_tid")]
        public Int32 ParentTid { get; set; }
        
        [JsonProperty("langCode")]
        public string LangCode { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("image")]
        public string WebImage { get; set; }

        [JsonProperty("image_data")]
        public string ImageData { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        public Image ThumbNail
        {
            get
            {
                return ImageManager.Base64ToImage(ImageData);
            }
        }
    }
}
