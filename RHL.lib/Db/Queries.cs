﻿using RHL.lib.Schema;
using System;
using System.Data.SQLite;

namespace RHL.lib.Db
{
    public static class Queries
    {

        #region Syncing
        internal static string UpdateLastSync
        {
            get
            {
                return string.Format("UPDATE sync SET last_sync = {0}", UnixTimeStamp);
            }
        }

        internal static Int32 UnixTimeStamp
        {
            get
            {
                return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }
        }

        internal static string GetLastSync
        {
            get
            {
                return "SELECT last_sync FROM sync";
            }
        }

        #endregion

        #region Table Creation 

        internal static string CreateSyncTable
        {
            get
            {
                return "CREATE TABLE sync(id INT PRIMARY KEY, last_sync INT); INSERT INTO sync (id, last_sync) VALUES (1, 0);";
            }
        }

        internal static string CreateLanguagesTable
        {
            get
            {
                return "CREATE TABLE languages(code TEXT, name TEXT)";
            }
        }

        internal static string CreateTermsTable
        {
            get
            {
                return "CREATE TABLE terms(lang_code TEXT, tid INT, parent INT, name TEXT, description TEXT, alias TEXT, image TEXT, PRIMARY KEY (lang_code, tid))";
            }
        }

        internal static string CreateArticlesTable
        {
            get
            {
                return "CREATE TABLE articles(lang_code TEXT, nid INT PRIMARY KEY, tid INT, title TEXT, published INT, updated INT, body TEXT, hash TEXT, image TEXT)";
            }
        }

        internal static string TruncateTermsTable
        {
            get
            {
                return "DELETE FROM terms";
            }
        }

        internal static string TruncateArticlesTable
        {
            get
            {
                return "DELETE FROM articles";
            }
        }

        #endregion

        #region Articles

        public static string InsertArticle(Article a)
        {
            return string.Format("INSERT INTO articles(lang_code, nid, tid, title, published, updated, body, hash, image) VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')",
                a.LangCode, a.Nid, a.Tid, a.Title, a.Published, a.Updated, a.Body, a.Hash, a.ImageData);
        }

        public static SQLiteCommand InsertArticleCommand(Article a)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = "INSERT OR IGNORE INTO articles(lang_code, nid, tid, title, published, updated, body, hash, image) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9)";
            cmd.Parameters.Add(new SQLiteParameter("@p1", a.LangCode));
            cmd.Parameters.Add(new SQLiteParameter("@p2", a.Nid));
            cmd.Parameters.Add(new SQLiteParameter("@p3", a.Tid));
            cmd.Parameters.Add(new SQLiteParameter("@p4", a.Title));
            cmd.Parameters.Add(new SQLiteParameter("@p5", a.Published));
            cmd.Parameters.Add(new SQLiteParameter("@p6", a.Updated));
            cmd.Parameters.Add(new SQLiteParameter("@p7", a.Body));
            cmd.Parameters.Add(new SQLiteParameter("@p8", a.Hash));
            cmd.Parameters.Add(new SQLiteParameter("@p9", a.ImageData));
            return cmd;
        }

        public static SQLiteCommand UpdateArticleCommand(Article a)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = "UPDATE articles SET lang_code = @p1, tid = @p3, title = @p4, published = @p5, updated = @p6, body = @p7, hash = @p8, image = @p9 WHERE nid = @p2";
            cmd.Parameters.Add(new SQLiteParameter("@p1", a.LangCode));
            cmd.Parameters.Add(new SQLiteParameter("@p2", a.Nid));
            cmd.Parameters.Add(new SQLiteParameter("@p3", a.Tid));
            cmd.Parameters.Add(new SQLiteParameter("@p4", a.Title));
            cmd.Parameters.Add(new SQLiteParameter("@p5", a.Published));
            cmd.Parameters.Add(new SQLiteParameter("@p6", a.Updated));
            cmd.Parameters.Add(new SQLiteParameter("@p7", a.Body));
            cmd.Parameters.Add(new SQLiteParameter("@p8", a.Hash));
            cmd.Parameters.Add(new SQLiteParameter("@p9", a.ImageData));
            return cmd;
        }

        internal static string GetArticles(string language, int tid, int limit)
        {
            string sql = string.Empty;
            if(tid == 0)
            {
                sql = string.Format("SELECT * FROM articles WHERE lang_code='{0}' ORDER BY published DESC", language);
            }
            else
            {
                sql = string.Format("SELECT * FROM articles WHERE lang_code='{0}' AND tid='{1}' ORDER BY published DESC", language, tid.ToString());
            }
            if(limit > 0)
            {
                sql = string.Format("{0} LIMIT {1}", sql, limit);
            }
            return sql;
        }

        internal static string GetArticleByHash(string hash)
        {
            return string.Format("SELECT id FROM articles WHERE hash = '{0}'", hash);
        }

        internal static string RemoveArticles(int tid, string lang_code)
        {
            return string.Format("DELETE FROM articles WHERE tid = {0} AND lang_code='{1}'", tid, lang_code);
        }

        internal static string RemoveTerms(string lang_code)
        {
            return string.Format("DELETE FROM terms WHERE lang_code='{0}'", lang_code);
        }

        #endregion

        #region Terms 

        internal static string InsertTerm(Term term)
        {
            return string.Format("INSERT OR IGNORE INTO terms (lang_code, tid, parent, name, description, alias, image) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", 
                term.LangCode, term.Tid, term.ParentTid, term.Name, term.Description, term.Alias, term.ImageData);
        }

        internal static string UpdateTerm(Term term)
        {
            return string.Format("UPDATE terms SET parent = '{2}', name = '{3}', description = '{4}', alias = '{5}', image = '{6}' WHERE tid = {1} AND lang_code = '{0}'",
                term.LangCode, term.Tid, term.ParentTid, term.Name, term.Description, term.Alias, term.ImageData);
        }
        
        internal static string GetTerms(string language, int parent)
        {
            return string.Format("SELECT * FROM terms WHERE lang_code = '{0}' AND parent = '{1}' ORDER BY name", language, parent.ToString());
        }

        internal static string GetTerm(string language, int tid)
        {
            return string.Format("SELECT * FROM terms WHERE lang_code = '{0}' AND tid = '{1}'", language, tid);
        }
        #endregion
    }
}
