﻿using Newtonsoft.Json;
using RHL.lib.Api;
using RHL.lib.Schema;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Net;

namespace RHL.lib.Db
{
    public class SyncData
    {
        private DbManager db;
        private ApiManager api;
        
        public SyncData()
        {
            db = new DbManager();
            api = new ApiManager();
        }

        public void SyncTerms(string lang_code)
        {
            // only perform this sync if we are online
            if(!hasConnection()) { return; }

            // find out when the last time we synced (this needs to be in euro time)
            int last_synced = db.GetLastSync();

            using (SQLiteConnection conn = new SQLiteConnection(db.ConnString))
            {
                conn.Open();
                string url = Urls.Terms(lang_code, last_synced);
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateSyncTable, conn))
                {
                    List<Term> terms = JsonConvert.DeserializeObject<List<Term>>(api.GET(Urls.Terms(lang_code, last_synced)));
                    terms.ForEach(a => db.SaveTerm(a));
                }
                conn.Close();
            }
        }
        
        public void SyncArticles(string lang_code = "all")
        {
            // only perform this sync if we are online
            if (!hasConnection()) { return; }

            // find out when the last time we synced
            int last_synced = db.GetLastSync();

            using (SQLiteConnection conn = new SQLiteConnection(db.ConnString))
            {
                conn.Open();

                using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateSyncTable, conn))
                {
                    // Get the articles for the term
                    List <Article> articles = JsonConvert.DeserializeObject<List<Article>>(api.GET(Urls.Articles(lang_code, last_synced)));
                    articles.ForEach(a => db.SaveArticle(a));
                }
                conn.Close();
            }
        }
        
        private bool hasConnection()
        {
            WebClient client = new WebClient();
            try
            {
                // If the English "terms" API URL is not available, then the API is down, or internet connection 
                // is not available
                // either way, this is now considered to be "offline"
                using (client.OpenRead("http://www.google.com"))
                {
                }
                return true;
            }
            catch (WebException)
            {
                return false;
            }

        }
    }
}
