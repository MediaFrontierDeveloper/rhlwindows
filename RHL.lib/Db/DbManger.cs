﻿using RHL.lib.Schema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Drawing;

namespace RHL.lib.Db
{
    public class DbManager
    {
        public DbManager()
        {

        }

        #region Paths

        public string DbName
        {
            get
            {
                return "rhl.sqlite";
            }
        }

        public string DbPath
        {
            get
            {
                return string.Format("{0}//{1}", Application.UserAppDataPath, DbName);
            }
        }

        internal string ConnString
        {
            get
            {
                return string.Format("Data Source={0};Version=3;", DbPath);
            }
        }

        #endregion

        #region Internal Methods 

        public void InitializeDb()
        {
            if(!File.Exists(DbPath))
            {
                SQLiteConnection.CreateFile(DbPath);

                using (SQLiteConnection conn = new SQLiteConnection(ConnString))
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateSyncTable ,conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateLanguagesTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateTermsTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateArticlesTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
            }
        }

        public int ExecuteNonQuery(string query)
        {
            int ret = 0;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                {
                    ret = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            return ret;
        }

        public int ExecuteNonQueryCommand(SQLiteCommand cmd)
        {
            int ret = 0;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                cmd.Connection = conn;
                using (cmd)
                {
                    ret = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            return ret;
        }

        internal DataTable ExecuteQuery(string query)
        {
            DataTable ret = new DataTable();

            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using(SQLiteDataAdapter dt = new SQLiteDataAdapter(query, conn))
                {
                    dt.Fill(ret);
                } 
                conn.Close();
            }

            return ret;
        }

        internal bool RowExists(string query)
        {
            bool ret = false;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                {
                    SQLiteDataReader rdr = cmd.ExecuteReader();
                    ret = rdr.HasRows;
                }
                conn.Close();
            }
            return ret;
        }

        #endregion

        #region Sync

        public Int32 GetLastSync()
        {
            int ret = 0;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.GetLastSync, conn))
                {
                    SQLiteDataReader rdr = cmd.ExecuteReader();
                   if(rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            ret = Convert.ToInt32(rdr["last_sync"]);
                        }

                    }
                }
                conn.Close();
            }
            return ret;
        }

        public void UpdateSync()
        {
            ExecuteNonQuery(Queries.UpdateLastSync);
        }

        #endregion

        #region Terms 

        internal void ClearTerms(string lang_code)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.RemoveTerms(lang_code), conn))
                {
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        internal void SaveTerm(Term term)
        {
            ExecuteNonQuery(Queries.InsertTerm(term));
            ExecuteNonQuery(Queries.UpdateTerm(term));
        }

        public List<Term> GetTerms(string language, int parent)
        {
            List<Term> ret = new List<Term>();
            DataTable terms = ExecuteQuery(Queries.GetTerms(language, parent));
            if (terms.Rows.Count > 0)
            {
                foreach (DataRow r in terms.Rows)
                {
                    Term t = new Term();
                    t.LangCode = (string)r["lang_code"];
                    t.Tid = (int)r["tid"];
                    t.ParentTid = (int)r["parent"];
                    t.Name = (string)r["name"];
                    t.Description = (string)r["description"];
                    t.Alias = (string)r["alias"];
                    t.ImageData = (string)r["image"];
                    ret.Add(t);
                }
            }
            return ret;
        }

        public Term GetTerm(string language, int tid, int parent = 0)
        {
            Term t = new Term();
            DataTable terms = ExecuteQuery(Queries.GetTerm(language, tid));
            if (terms.Rows.Count == 1)
            {
                DataRow r = terms.Rows[0];
                t.LangCode = (string)r["lang_code"];
                t.Tid = (int)r["tid"];
                t.Name = (string)r["name"];
                t.Description = (string)r["description"];
                t.Alias = (string)r["alias"];
                t.ImageData = (string)r["image"];
            }
            return t;
        }

        public void RemoveTerms(string lang_code)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.RemoveTerms(lang_code), conn))
                {
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        #endregion

        #region Articles 

        public List<Article> GetArticles(string language, int tid, int limit = 0)
        {
            List<Article> ret = new List<Article>();
            DataTable articles = ExecuteQuery(Queries.GetArticles(language, tid, limit));
            if (articles.Rows.Count > 0)
            {
                foreach (DataRow r in articles.Rows)
                {
                    Article a = new Article();
                    a.LangCode = (string)r["lang_code"];
                    a.Nid = (int)r["nid"];
                    a.Tid = (int)r["tid"];
                    a.Title = (string)r["title"];
                    a.Body = (string)r["body"];
                    a.ImageData = r.IsNull("image") ? "" : (string)r["image"];
                    a.Published = (int)r["published"];
                    a.Updated = (int)r["updated"];
                    a.Hash = (string)r["hash"];
                    ret.Add(a);
                }
            }
            return ret;
        }

        public void RemoveArticles(int tid, string lang_code)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.RemoveArticles(tid, lang_code), conn))
                {
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        public void SaveArticle(Article article)
        {
            ExecuteNonQueryCommand(Queries.InsertArticleCommand(article));
            ExecuteNonQueryCommand(Queries.UpdateArticleCommand(article));
        }

        #endregion
    }
}
