﻿namespace RHL.lib.Api
{
    public class Urls
    {
        internal const string BASE_URL = "http://46.101.209.4/rhlapi2";
        
        internal static string Terms(string language, int timestamp)
        {
            return string.Format("{0}/{1}/terms/{2}/sync", BASE_URL, language, timestamp.ToString());
        }

        public static string Articles(string lang_code, int timestamp)
        {
            return string.Format("{0}/{1}/articles/{2}/sync", BASE_URL, lang_code, timestamp.ToString());
        }
    }
}