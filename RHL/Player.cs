﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RHL.assets;
using RHL.lib.Db;
using RHL.lib.Schema;
using RHL.lib.Text;

namespace RHL
{
    public partial class Player : Form
    {
        private DbManager _db = DbManager.Instance;

        public Player()
        {
            InitializeComponent();
        }

        public void LoadVideo(int nid)
        {
            Video vid = _db.GetVideo(nid);
            uxDescription.Text = vid.Body;
            uxBrowser.DocumentText = Template.YouTubePlayer(vid.VideoUrl);
            uxFav.LoadFav(vid.Type, vid.Nid);
        }
    }
}
