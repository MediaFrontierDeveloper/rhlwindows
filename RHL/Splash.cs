﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RHL.lib.Db;
using RHL.lib.Sync;
using RHL.lib.Messages;


namespace RHL
{
    public partial class Splash : Form
    {
        private DbManager _db;
        private SyncManager _sd;
        private Sync _sync;

        public Splash()
        {
            InitializeComponent();
            _db = DbManager.Instance;
            _sd = SyncManager.Instance;
            _sync = new Sync();
            _sync.SyncDone += Sync_Done;
            AppMessage.Get.PropertyChanged += Get_PropertyChanged;
        }

        private void Get_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            uxSyncMessage.Text = AppMessage.Get.MessageText;
            this.Refresh();
        }

        private void Splash_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            Cursor.Current = Cursors.WaitCursor;
            _sync.DoSync(true);
        }

        private void Sync_Done(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.Default;
                RHLApp2 rhl = new RHLApp2();
                rhl.LoadGuidelines();
                rhl.LoadVideos();
                this.Hide();
                rhl.FormClosed += (s, args) => this.Close();
                rhl.Show();
            }
            catch
            {
                //TODO: Remove this
                //throw;
            }
            finally
            {
                _db = null;
                _sd = null;
                _sync = null;
            }
        }
    }
}
