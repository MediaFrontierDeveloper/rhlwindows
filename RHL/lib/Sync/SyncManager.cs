﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Linq;
using Newtonsoft.Json;
using RHL.lib.Api;
using RHL.lib.Db;
using RHL.lib.Schema;
using RHL.lib.Messages;

namespace RHL.lib.Sync
{
    public class SyncManager
    {
        #region Contructor / Initilization

        private DbManager db;
        private ApiManager api;

        static readonly SyncManager _instance = new SyncManager();

        public static SyncManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public SyncManager()
        {
            db = DbManager.Instance;
            api = ApiManager.Instance;
        }

        public bool HasConnection()
        {
            // This is essentially just a wrapper 
            // for the same ApiManager method
            return api.HasConnection();
        }

        #endregion

        #region Terms 

        public void SyncTerms(string lang_code)
        {
            // only perform this sync if we are online
            if(!api.HasConnection()) { return; }

            // find out when the last time we synced (this needs to be in euro time)
            int last_synced = db.GetLastSync();

            using (SQLiteConnection conn = new SQLiteConnection(db.ConnString))
            {
                conn.Open();
                string url = Urls.Terms(lang_code, last_synced);
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateSyncTable, conn))
                {
                    List<Term> terms = JsonConvert.DeserializeObject<List<Term>>(api.GET(Urls.Terms(lang_code, last_synced)));
                    terms.ForEach(t => {
                        AppMessage.Get.MessageText = string.Format("Downloading: ({0}) term {1} of {2}", lang_code, terms.IndexOf(t), terms.Count);
                        db.SaveTerm(t);
                    });
                }
                conn.Close();
            }
        }

        #endregion

        #region Articles 

        public void SyncArticles(string lang_code = "all")
        {
            // only perform this sync if we are online
            if (!api.HasConnection()) { return; }

            // find out when the last time we synced
            int last_synced = db.GetLastSync();

            // Get the articles for the term
            List <Article> articles = JsonConvert.DeserializeObject<List<Article>>(api.GET(Urls.Articles(lang_code, last_synced)));
            articles.ForEach(a => {
                AppMessage.Get.MessageText = string.Format("Downloading ({0}) article {1} of {2}", lang_code, articles.IndexOf(a), articles.Count);
                db.SaveArticle(a);
            });

            // Get the list of current active nodes
            // This is a giant array of node ids for this type (article)
            // and language code (e.g. "en")
            int[] active = JsonConvert.DeserializeObject<int[]>(api.GET(Urls.ArticlesActive(lang_code)));
            db.RemoveArticles(active, lang_code);
        }

        #endregion

        #region New In RHL

        public void ClearNew()
        {
            // only perform this sync if we are online
            if (!api.HasConnection()) { return; }

            // It doesn't matter about the last time we synced, because the dataset 
            // for this table is small enough that the table can be cleared and recreated
            // Also - there will be a lot of deletions anyway (it mirrors the front page)
            // So this is actually a more efficent approach
            using (SQLiteConnection conn = new SQLiteConnection(db.ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.TruncateNewTable, conn))
                {
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        public void SyncNew(string lang_code)
        {
            // only perform this sync if we are online
            if (!api.HasConnection()) { return; }

            List<Node> nodes = JsonConvert.DeserializeObject<List<Node>>(api.GET(Urls.New(lang_code)));
            nodes.ForEach(n => db.SaveNew(n));
        }

        #endregion

        #region Videos

        public void SyncVideos(string lang_code = "all", bool resync = false)
        {
            // only perform this sync if we are online
            if (!api.HasConnection()) { return; }

            // find out when the last time we synced
            int last_synced = db.GetLastSync();

            if (resync)
                last_synced = 0;

            // Get the articles for the term
            AppMessage.Get.MessageText = "Downloading Videos";
            List<Video> videos = JsonConvert.DeserializeObject<List<Video>>(api.GET(Urls.Videos(lang_code, last_synced)));
            videos.ForEach(v => {
                AppMessage.Get.MessageText = string.Format("Downloading ({0}) video {1} of {2}", lang_code, videos.IndexOf(v), videos.Count);
                db.SaveVideo(v);
            });

            int[] active = JsonConvert.DeserializeObject<int[]>(api.GET(Urls.VideosActive(lang_code)));
            db.RemoveVideos(active, lang_code);
        }

        #endregion

        #region Guidelines

        public void SyncGuidelines(string lang_code = "all", bool resync = false)
        {
            // only perform this sync if we are online
            if (!api.HasConnection()) { return; }

            // find out when the last time we synced
            int last_synced = db.GetLastSync();

            if (resync)
                last_synced = 0;

            // Get the articles for the term
            List<Guideline> guides = JsonConvert.DeserializeObject<List<Guideline>>(api.GET(Urls.Guidelines(lang_code, last_synced)));
            guides.ForEach(g => {
                AppMessage.Get.MessageText = string.Format("Downloading: ({0}) guideline {1} of {2}", lang_code, guides.IndexOf(g), guides.Count);
                db.SaveGuideline(g); 
            });

            int[] active = JsonConvert.DeserializeObject<int[]>(api.GET(Urls.GuidelinesActive(lang_code)));
           // db.RemoveGuidelines(active, lang_code);
        }

        #endregion
    }
}
