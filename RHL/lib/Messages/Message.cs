﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RHL.lib.Messages
{
    public class AppMessage : INotifyPropertyChanged
    {
        private string _messageText;

        public string MessageText
        {
            get
            {
                return this._messageText;
            }
            set
            {
                if (this._messageText == value)
                {
                    return;
                }

                this._messageText = value;

                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("MessageText"));
                }
            }
        }

    public event PropertyChangedEventHandler PropertyChanged;

        private AppMessage() { }

        public static readonly AppMessage Get = new AppMessage();
    }
}
