﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RHL.lib.Text
{
    public class Parser
    {
        public static string ScrubHtml(string value)
        {
            if(!string.IsNullOrEmpty(value))
            {
                var step1 = Regex.Replace(value, @"<[^>]+>|&nbsp;", "").Trim();
                var step2 = Regex.Replace(step1, @"\s{2,}", " ");
                return step2;
            }
            return string.Empty;
        }
    }
}
