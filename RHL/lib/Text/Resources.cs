﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace RHL.lib.Text
{
    class Resources
    {
        internal static string GetString(string str, CultureInfo culture)
        {
            string lang = culture.Name;
            switch(lang.ToLower())
            {
                case 'en':

            }
        }

        internal Dictionary<string, string> En()
        {
            Dictionary<string, string> en = new Dictionary<string,string>();
            en.Add("title_rhl ", @"RH ");
            en.Add("action_search ", @"Search");
            en.Add("action_about", @"About RHL ");
            en.Add("library_title ", @"Library ");
            en.Add("videos_title", @"Videos");
            en.Add("guidelines_title", @"Guidelines");
            en.Add("favourites_title", @"Favourites");
            en.Add("search_string ", @"Results: Sections, Articles, Guidelines, Videos ");
            en.Add("download_error_title", @"Something went wrong");
            en.Add("download_error_message", @"A stable internet connection is needed to predownload the whole library. There was an error when predownloading the library. Tap 'Try again' if you want to start the download again or 'Exit' if you want to try later.");
            en.Add("download_error_try_again", @"Try again ");
            en.Add("download_error_exit ", @"Exit");
            en.Add("pdf_error_title ", @"Something went wrong");
            en.Add("pdf_error_not_found_message ", @"This guideline is currently unavailable ");
            en.Add("pdf_error_corrupt_message ", @"Guideline file is broken");
            en.Add("pdf_error_ok", @"Ok");
            en.Add("section_format", @"Hello World from section: ");
            en.Add("action_settings ", @"Settings");
            en.Add("settings_language ", @"Language");
            en.Add("settings_auto_download", @"Auto Download ");
            en.Add("settings_auto_download_all", @"All ");
            en.Add("article_link_read_more", @"Read More ");
            en.Add("about_title_1 ", @"ACKNOWLEDGEMENTS");
            en.Add("about_title_2 ", @"What is RHL?");
            en.Add("about_title_3 ", @"Application design and development by Media Frontier");
            en.Add("about_text_1", @"The WHO Reproductive Health Library (RHL) is a curated collection of high-quality evidence in reproductive health, published by the World Health Organization

RHL’s mission is to accelerate the implementation of new, high-quality evidence into reproductive health practice globally.

To achieve this, we systematically identify and summarize new evidence in reproductive health, and disseminate it globally. RHL strives for universal accessibility, to ensure high-quality evidence is available to all who need it.

RHL is published and co-ordinated by staff at the Department of Reproductive Health and Research at the World Health Organization in Geneva. We are guided by the RHL Editorial Board, an international group of reproductive health experts, clinicians and researchers. The RHL Community comprises an international group of volunteers who are working in all areas of sexual and reproductive health practice, research and dissemination.");

            en.Add("about_text_2", @"The WHO Reproductive Health Library (RHL) project was initiated by the UNDP/UNFPA/UNICEF/WHO/World Bank Special Programme of Research, Development and Research Training in Human Reproduction (HRP) in April 1996. Since then many individuals and institutions have supported and contributed to this project. The World Health Organization (WHO) and the Department of Reproductive Health and Research, of which HRP is now a part, are grateful to all those individuals and institutions.

WHO is especially grateful to Dr Iain Chalmers (The James Lind Initiative) for his supportive and thoughtful advice, which has helped to give the project its current direction. WHO would also like to thank Dr José Belizan, Dr Paul Garner, Dr James Neilson and the late Dr Chris Silagy for their support and helpful advice during the initial development of the RHL project.

WHO is grateful to John Wiley & Sons, Ltd. and the Cochrane Collaboration for allowing WHO to reproduce Cochrane reviews in RHL. Thanks are also due to the Centre for Reviews and Dissemination, University of York, York, United Kingdom, for giving permission to include in RHL a selection of their structured abstracts (DARE abstracts). WHO would also like to thank BioMed Central Ltd., Blackwell Science Ltd. and Elsevier Inc. for giving permission to reproduce in RHL selected articles from their journals. WHO would like to thank the Department for International Development, London, United Kingdom, and the Ministry of Health, Spain, for their financial contributions to the RHL project.");
            en.Add("about_text_3", @"Media Frontier is a creative digital agency based in Geneva. We specialise in application design and development. If you\'ve got any comments on this application, or would like to provide feedback on functionality then please contact info@mediafrontier.ch.");
            en.Add("library_sections_upcase ", @"LIBRARY SECTIONS");
            en.Add("library_upcase", @"LIBRARY ");
            en.Add("videos_upcase ", @"VIDEOS");
            en.Add("guidelines_upcase ", @"GUIDELINES");
            en.Add("new_in_rhl", @"NEW IN RHL");
            en.Add("contents", @"CONTENTS");
            en.Add("subsections ", @"SUBSECTIONS ");
            en.Add("share ", @"Share ");
            en.Add("search_results_upcase ", @"SEARCH RESULTS");
            en.Add("add_subsection_to_favourites", @"Add subsection to favourites");
            en.Add("open_in_external_app", @"Open in external app");
        }
    }
}
