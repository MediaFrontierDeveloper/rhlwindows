﻿using RHL.lib.Schema;
using System;
using System.Data;
using System.Data.SQLite;
using System.Text;

namespace RHL.lib.Db
{
    public static class Queries
    {

        #region Syncing
        internal static string UpdateLastSync(int timestamp = 0)
        {
            if(timestamp < 1 )
            {
                return string.Format("UPDATE sync SET last_sync = {0}", UnixTimeStamp);
            }
            else
            {
                return string.Format("UPDATE sync SET last_sync = {0}", timestamp);
            }
        }

        public static Int32 UnixTimeStamp
        {
            get
            {
                return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            }
        }

        internal static string GetLastSync
        {
            get
            {
                return "SELECT last_sync FROM sync";
            }
        }

        #endregion

        #region Table Creation 

        internal static string CreateSyncTable
        {
            get
            {
                return "CREATE TABLE sync(id INT PRIMARY KEY, last_sync INT); INSERT INTO sync (id, last_sync) VALUES (1, 0);";
            }
        }

        internal static string CreateLanguagesTable
        {
            get
            {
                return "CREATE TABLE languages(code TEXT, name TEXT)";
            }
        }

        internal static string CreateTermsTable
        {
            get
            {
                return "CREATE TABLE terms(lang_code TEXT, tid INT, parent INT, name TEXT, description TEXT, alias TEXT, image TEXT, sort_order INT, PRIMARY KEY (lang_code, tid))";
            }
        }

        internal static string CreateArticlesTable
        {
            get
            {
                return "CREATE TABLE articles(lang_code TEXT, nid INT PRIMARY KEY, tid INT, title TEXT, published INT, updated INT, body TEXT, hash TEXT, image TEXT, type TEXT DEFAULT 'rhl_content_article')";
            }
        }

        internal static string CreateFavsTable
        {
            get
            {
                return "CREATE TABLE favs(type TEXT, nid INT, added DATETIME DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(type, nid))";
            }
        }

        internal static string CreateNewTable
        {
            get
            {
                return "CREATE TABLE new(id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT, nid INT, lang_code TEXT)";
            }
        }

        internal static string TruncateNewTable
        {
            get
            {
                return "DELETE FROM new";
            }
        }

        internal static string TruncateTermsTable
        {
            get
            {
                return "DELETE FROM terms";
            }
        }

        internal static string TruncateArticlesTable
        {
            get
            {
                return "DELETE FROM articles";
            }
        }

        #endregion

        #region Articles

        public static string InsertArticle(Article a)
        {
            return string.Format("INSERT INTO articles(lang_code, nid, tid, title, published, updated, body, hash, image) VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')",
                a.LangCode, a.Nid, a.Tid, a.Title, a.Published, a.Updated, a.Body, a.Hash, a.ImageData);
        }

        public static SQLiteCommand InsertArticleCommand(Article a)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = "INSERT OR IGNORE INTO articles(lang_code, nid, tid, title, published, updated, body, hash, image) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9)";
            cmd.Parameters.Add(new SQLiteParameter("@p1", a.LangCode));
            cmd.Parameters.Add(new SQLiteParameter("@p2", a.Nid));
            cmd.Parameters.Add(new SQLiteParameter("@p3", a.Tid));
            cmd.Parameters.Add(new SQLiteParameter("@p4", a.Title));
            cmd.Parameters.Add(new SQLiteParameter("@p5", a.Published));
            cmd.Parameters.Add(new SQLiteParameter("@p6", a.Updated));
            cmd.Parameters.Add(new SQLiteParameter("@p7", a.Body));
            cmd.Parameters.Add(new SQLiteParameter("@p8", a.Hash));
            cmd.Parameters.Add(new SQLiteParameter("@p9", a.ImageData));
            return cmd;
        }

        public static SQLiteCommand UpdateArticleCommand(Article a)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = "UPDATE articles SET lang_code = @p1, tid = @p3, title = @p4, published = @p5, updated = @p6, body = @p7, hash = @p8, image = @p9 WHERE nid = @p2";
            cmd.Parameters.Add(new SQLiteParameter("@p1", a.LangCode));
            cmd.Parameters.Add(new SQLiteParameter("@p2", a.Nid));
            cmd.Parameters.Add(new SQLiteParameter("@p3", a.Tid));
            cmd.Parameters.Add(new SQLiteParameter("@p4", a.Title));
            cmd.Parameters.Add(new SQLiteParameter("@p5", a.Published));
            cmd.Parameters.Add(new SQLiteParameter("@p6", a.Updated));
            cmd.Parameters.Add(new SQLiteParameter("@p7", a.Body));
            cmd.Parameters.Add(new SQLiteParameter("@p8", a.Hash));
            cmd.Parameters.Add(new SQLiteParameter("@p9", a.ImageData));
            return cmd;
        }

        internal static string GetArticles(string language, int tid, int limit)
        {
            string sql = string.Empty;
            if(tid == 0)
            {
                sql = string.Format("SELECT a.*, f.nid as fav FROM articles a LEFT OUTER JOIN favs f ON(a.nid = f.nid AND f.type = 'rhl_content_article') WHERE a.lang_code='{0}' ORDER BY a.published DESC", language);
            }
            else
            {
                sql = string.Format("SELECT a.*, f.nid as fav FROM articles a LEFT OUTER JOIN favs f ON(a.nid = f.nid AND f.type = 'rhl_content_article') WHERE a.lang_code='{0}' AND a.tid='{1}' ORDER BY a.published DESC", language, tid.ToString());
            }
            if(limit > 0)
            {
                sql = string.Format("{0} LIMIT {1}", sql, limit);
            }
            return sql;
        }

        internal static string SearchArticles(string language, int tid, int limit, string terms)
        {
            string sql = string.Empty;
            if (tid == 0)
            {
                sql = string.Format("SELECT a.*, f.nid as fav FROM articles a LEFT OUTER JOIN favs f ON(a.nid = f.nid AND f.type = 'rhl_content_article') WHERE a.lang_code='{0}' AND LOWER(title) LIKE '%{1}%' ORDER BY a.published DESC", language, terms);
            }
            else
            {
                sql = string.Format("SELECT a.*, f.nid as fav FROM articles a LEFT OUTER JOIN favs f ON(a.nid = f.nid AND f.type = 'rhl_content_article') WHERE a.lang_code='{0}' AND a.tid='{1}' AND LOWER(title) LIKE '%{2}%' ORDER BY a.published DESC", language, tid.ToString(), terms);
            }
            if (limit > 0)
            {
                sql = string.Format("{0} LIMIT {1}", sql, limit);
            }
            return sql;
        }

        internal static string GetFavArticles()
        {
            return "SELECT articles.*, '1' AS fav FROM favs JOIN articles ON favs.nid = articles.nid WHERE favs.type = 'rhl_content_article'";
        }

        internal static string GetArticleByHash(string hash)
        {
            return string.Format("SELECT id FROM articles WHERE hash = '{0}'", hash);
        }

        internal static string RemoveArticles(int tid, string lang_code)
        {
            return string.Format("DELETE FROM articles WHERE tid = {0} AND lang_code='{1}'", tid, lang_code);
        }

        internal static string RemoveTerms(string lang_code)
        {
            return string.Format("DELETE FROM terms WHERE lang_code='{0}'", lang_code);
        }

        #endregion

        #region Terms 

        internal static string InsertTerm(Term term)
        {
            return string.Format("INSERT OR IGNORE INTO terms (lang_code, tid, parent, name, description, alias, image, sort_order) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}', '{7}')", 
                term.LangCode, term.Tid, term.ParentTid, term.Name, term.Description.Replace("'", "''"), term.Alias, term.ImageData, term.SortOrder);
        }

        internal static string UpdateTerm(Term term)
        {
            return string.Format("UPDATE terms SET parent = '{2}', name = '{3}', description = '{4}', alias = '{5}', image = '{6}', sort_order = '{7}' WHERE tid = {1} AND lang_code = '{0}'",
                term.LangCode, term.Tid, term.ParentTid, term.Name, term.Description.Replace("'", "''"), term.Alias, term.ImageData, term.SortOrder);
        }
        
        internal static string GetTerms(string language, int parent)
        {
            return string.Format("SELECT * FROM terms WHERE lang_code = '{0}' AND parent = '{1}' ORDER BY sort_order, name", language, parent.ToString());
        }

        internal static string GetTerm(string language, int tid)
        {
            return string.Format("SELECT * FROM terms WHERE lang_code = '{0}' AND tid = '{1}'", language, tid);
        }
        #endregion

        #region Favourites

        internal static string GetFavs()
        {
            return "SELECT * FROM favs ORDER BY added DESC";
        }

        internal static string GetFav(string type, int nid)
        {
            return string.Format("SELECT * FROM favs WHERE type='{0}' AND nid = {1} LIMIT 1", type, nid);
        }

        internal static string DeleteFav(string type, int nid)
        {
            return string.Format("DELETE FROM favs WHERE type='{0}' AND nid = {1}", type, nid);
        }

        internal static string InsertFav(string type, int nid)
        {
            return string.Format("INSERT INTO favs (type, nid) VALUES('{0}', {1})", type, nid);
        }

        internal static string FavsCount()
        {
            return "SELECT COUNT(*) AS fav_count FROM favs";
        }

        #endregion

        #region New

        internal static string GetNew(string lang_code)
        {
            string query = @"SELECT 
                        new.id,
                        new.lang_code,
                        new.nid,
                        new.type,
                        t.tid,
                        t.title,
                        t.body,
                        t.image,
                        t.hash,
                        t.published,
                        t.updated
                        FROM new
                        JOIN articles t ON(new.nid = t.nid)
                        WHERE new.lang_code = '{0}'
                        UNION ALL
                        SELECT
                        new.id,
                        new.lang_code,
                        new.nid,
                        new.type,
                        t.tid,
                        t.title,
                        t.body,
                        t.image,
                        t.hash,
                        t.published,
                        t.updated
                        FROM new
                        JOIN guidelines t ON(new.nid = t.nid)
                        WHERE new.lang_code = '{0}'
                        UNION ALL
                        SELECT
                        new.id,
                        new.lang_code,
                        new.nid,
                        new.type,
                        t.tid,
                        t.title,
                        t.body,
                        t.image,
                        t.hash,
                        t.published,
                        t.updated
                        FROM new
                        JOIN videos t ON(new.nid = t.nid)
                        WHERE new.lang_code = '{0}'
                        ORDER BY 1";
            return string.Format(query, lang_code);
        }

        internal static string InsertNew(string type, int nid, string lang_code)
        {
            return string.Format("INSERT INTO new (type, nid, lang_code) VALUES('{0}', {1}, '{2}')", type, nid, lang_code);
        }

        #endregion

        #region Videos

        internal class Videos
        {
            internal static string CreateTable
            {
                get
                {
                    return "CREATE TABLE videos(lang_code TEXT, nid INT PRIMARY KEY, tid INT, title TEXT, published INT, updated INT, body TEXT, hash TEXT, image TEXT, type TEXT DEFAULT 'video', video_url TEXT, sticky INT)";
                }
            }

            internal static string Get(string language, int tid, bool favsOnly)
            {
                StringBuilder sql = new StringBuilder();
                if(favsOnly)
                {
                    sql.Append("SELECT videos.* FROM favs JOIN videos ON favs.nid = videos.nid AND favs.type = videos.type");
                }
                else
                {
                    sql.AppendFormat("SELECT * FROM videos WHERE lang_code = '{0}' ORDER BY sticky ASC, title ASC, published DESC", language);
                    if (tid > 0)
                    {
                        sql.AppendFormat(" AND tid = {0}", tid);
                    }
                }
                return sql.ToString();
            }

            internal static string Search(string language, int tid, bool favsOnly, string terms)
            {
                StringBuilder sql = new StringBuilder();
                if (favsOnly)
                {
                    sql.Append("SELECT videos.* FROM favs JOIN videos ON favs.nid = videos.nid AND favs.type = videos.type");
                }
                else
                {
                    sql.AppendFormat("SELECT * FROM videos WHERE lang_code = '{0}' AND title LIKE '%{1}%'", language, terms);
                    if (tid > 0)
                    {
                        sql.AppendFormat(" AND tid = {0}", tid);
                    }
                }
                return sql.ToString();
            }

            internal static string Get(int nid)
            {
                return string.Format("SELECT * FROM videos WHERE nid = {0} LIMIT 1", nid);
            }

            internal static SQLiteCommand Insert(Video v)
            {
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.CommandText = "INSERT OR IGNORE INTO videos(lang_code, nid, tid, title, published, updated, body, hash, image, video_url, sticky) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11)";
                cmd.Parameters.Add(new SQLiteParameter("@p1", v.LangCode));
                cmd.Parameters.Add(new SQLiteParameter("@p2", v.Nid));
                cmd.Parameters.Add(new SQLiteParameter("@p3", v.Tid));
                cmd.Parameters.Add(new SQLiteParameter("@p4", v.Title));
                cmd.Parameters.Add(new SQLiteParameter("@p5", v.Published));
                cmd.Parameters.Add(new SQLiteParameter("@p6", v.Updated));
                cmd.Parameters.Add(new SQLiteParameter("@p7", v.Body));
                cmd.Parameters.Add(new SQLiteParameter("@p8", v.Hash));
                cmd.Parameters.Add(new SQLiteParameter("@p9", v.ImageData));
                cmd.Parameters.Add(new SQLiteParameter("@p10", v.VideoUrl));
                cmd.Parameters.Add(new SQLiteParameter("@p11", v.Sticky));
                return cmd;
            }

            internal static SQLiteCommand Update(Video v)
            {
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.CommandText = "UPDATE videos SET lang_code = @p1, tid = @p3, title = @p4, published = @p5, updated = @p6, body = @p7, hash = @p8, image = @p9, video_url = @p10, sticky = @p11 WHERE nid = @p2";
                cmd.Parameters.Add(new SQLiteParameter("@p1", v.LangCode));
                cmd.Parameters.Add(new SQLiteParameter("@p2", v.Nid));
                cmd.Parameters.Add(new SQLiteParameter("@p3", v.Tid));
                cmd.Parameters.Add(new SQLiteParameter("@p4", v.Title));
                cmd.Parameters.Add(new SQLiteParameter("@p5", v.Published));
                cmd.Parameters.Add(new SQLiteParameter("@p6", v.Updated));
                cmd.Parameters.Add(new SQLiteParameter("@p7", v.Body));
                cmd.Parameters.Add(new SQLiteParameter("@p8", v.Hash));
                cmd.Parameters.Add(new SQLiteParameter("@p9", v.ImageData));
                cmd.Parameters.Add(new SQLiteParameter("@p10", v.VideoUrl));
                cmd.Parameters.Add(new SQLiteParameter("@p11", v.Sticky));
                return cmd;
            }
        }

        #endregion

        #region Guidelines

        internal class Guidelines
        {
            internal static string CreateTable
            {
                get
                {
                    return "CREATE TABLE guidelines(lang_code TEXT, nid INT PRIMARY KEY, tid INT, title TEXT, published INT, updated INT, body TEXT, hash TEXT, image TEXT, type TEXT DEFAULT 'guideline', guide_url TEXT, guide_title TEXT, guide_file BLOB)";
                }
            }

            internal static string Get(string language, int tid, bool favsOnly)
            {
                StringBuilder sql = new StringBuilder();
                if (favsOnly)
                {
                    sql.Append("SELECT guidelines.* FROM favs JOIN guidelines ON favs.nid = guidelines.nid AND favs.type = guidelines.type");
                }
                else
                {
                    sql.AppendFormat("SELECT * FROM guidelines WHERE lang_code = '{0}' ORDER BY published DESC", language);
                    if (tid > 0)
                    {
                        sql.AppendFormat(" AND tid = {0}", tid);
                    }
                }
                return sql.ToString();
            }

            internal static string Search(string language, string terms)
            {
                StringBuilder sql = new StringBuilder();
                sql.AppendFormat("SELECT * FROM guidelines WHERE lang_code = '{0}' AND LOWER(title) LIKE '%{1}%' ORDER BY published DESC", language, terms);
                return sql.ToString();
            }

            internal static string Get(int nid)
            {
                return string.Format("SELECT * FROM guidelines WHERE nid = {0} LIMIT 1", nid);
            }

            internal static SQLiteCommand Insert(Guideline g)
            {
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.CommandText = "INSERT OR IGNORE INTO guidelines(lang_code, nid, tid, title, published, updated, body, hash, image, guide_url, guide_title, guide_file) VALUES(@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12)";
                cmd.Parameters.Add(new SQLiteParameter("@p1", g.LangCode));
                cmd.Parameters.Add(new SQLiteParameter("@p2", g.Nid));
                cmd.Parameters.Add(new SQLiteParameter("@p3", g.Tid));
                cmd.Parameters.Add(new SQLiteParameter("@p4", g.Title));
                cmd.Parameters.Add(new SQLiteParameter("@p5", g.Published));
                cmd.Parameters.Add(new SQLiteParameter("@p6", g.Updated));
                cmd.Parameters.Add(new SQLiteParameter("@p7", g.Body));
                cmd.Parameters.Add(new SQLiteParameter("@p8", g.Hash));
                cmd.Parameters.Add(new SQLiteParameter("@p9", g.ImageData));
                cmd.Parameters.Add(new SQLiteParameter("@p10", g.GuideUrl));
                cmd.Parameters.Add(new SQLiteParameter("@p11", g.GuideTitle));
                cmd.Parameters.Add(new SQLiteParameter("@p12", g.DownloadGuide()));
                return cmd;
            }

            internal static SQLiteCommand Update(Guideline g)
            {
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.CommandText = "UPDATE guidelines SET lang_code = @p1, tid = @p3, title = @p4, published = @p5, updated = @p6, body = @p7, hash = @p8, image = @p9, guide_url = @p10, guide_title=@p11, guide_file=@p12 WHERE nid = @p2";
                cmd.Parameters.Add(new SQLiteParameter("@p1", g.LangCode));
                cmd.Parameters.Add(new SQLiteParameter("@p2", g.Nid));
                cmd.Parameters.Add(new SQLiteParameter("@p3", g.Tid));
                cmd.Parameters.Add(new SQLiteParameter("@p4", g.Title));
                cmd.Parameters.Add(new SQLiteParameter("@p5", g.Published));
                cmd.Parameters.Add(new SQLiteParameter("@p6", g.Updated));
                cmd.Parameters.Add(new SQLiteParameter("@p7", g.Body));
                cmd.Parameters.Add(new SQLiteParameter("@p8", g.Hash));
                cmd.Parameters.Add(new SQLiteParameter("@p9", g.ImageData));
                cmd.Parameters.Add(new SQLiteParameter("@p10", g.GuideUrl));
                cmd.Parameters.Add(new SQLiteParameter("@p11", g.GuideTitle));
                cmd.Parameters.Add(new SQLiteParameter("@p12", g.DownloadGuide()));
                return cmd;
            }
        }

        #endregion
    }
}
