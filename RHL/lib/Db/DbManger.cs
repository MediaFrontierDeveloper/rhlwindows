﻿using RHL.lib.Schema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Net;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections;

namespace RHL.lib.Db
{
    public sealed class DbManager
    {
        public DbManager() { }

        static readonly DbManager _instance = new DbManager();

        public static DbManager Instance
        {
            get
            {
                return _instance;
            }
        }

        #region Paths

        public string DbName
        {
            get
            {
                return "rhl.sqlite";
            }
        }

        public string DbPath
        {
            get
            {
                return string.Format("{0}//{1}", Application.UserAppDataPath, DbName);
            }
        }

        internal string ConnString
        {
            get
            {
                return string.Format("Data Source={0};Version=3;", DbPath);
            }
        }

        #endregion

        #region Internal Methods 

        public void InitializeDb()
        {
            if(!File.Exists(DbPath))
            {
                SQLiteConnection.CreateFile(DbPath);

                using (SQLiteConnection conn = new SQLiteConnection(ConnString))
                {
                    conn.Open();
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateSyncTable ,conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateLanguagesTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateTermsTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateArticlesTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateFavsTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.CreateNewTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(Queries.Videos.CreateTable, conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
            }
        }

        public int ExecuteNonQuery(string query)
        {
            int ret = 0;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                {
                    ret = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            return ret;
        }

        public int ExecuteNonQueryCommand(SQLiteCommand cmd)
        {
            int ret = 0;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                cmd.Connection = conn;
                using (cmd)
                {
                    ret = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            return ret;
        }

        internal DataTable ExecuteQuery(string query)
        {
            DataTable ret = new DataTable();

            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using(SQLiteDataAdapter dt = new SQLiteDataAdapter(query, conn))
                {
                    dt.Fill(ret);
                } 
                conn.Close();
            }

            return ret;
        }

        internal bool RowExists(string query)
        {
            bool ret = false;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(query, conn))
                {
                    SQLiteDataReader rdr = cmd.ExecuteReader();
                    ret = rdr.HasRows;
                }
                conn.Close();
            }
            return ret;
        }

        static byte[] GetBytes(SQLiteDataReader reader)
        {
            const int CHUNK_SIZE = 2 * 1024;
            byte[] buffer = new byte[CHUNK_SIZE]; 
            long bytesRead;
            long fieldOffset = 0;
            using (MemoryStream stream = new MemoryStream())
            {
                while ((bytesRead = reader.GetBytes(0, fieldOffset, buffer, 0, buffer.Length)) > 0)
                {
                    stream.Write(buffer, 0, (int)bytesRead);
                    fieldOffset += bytesRead;
                }
                return stream.ToArray();
            }
        }

        #endregion

        #region Sync

        public Int32 GetLastSync()
        {
            int ret = 0;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.GetLastSync, conn))
                {
                    SQLiteDataReader rdr = cmd.ExecuteReader();
                   if(rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            // Important!  As people could be in differing timezones
                            // There is potential that clients could be ahead of the 
                            // timezone of the server (Drupal saves in UTC)
                            // We could just take the last 24 hours of data
                            // to save messing around here working out timezone differences
                            // but normally the dataset should be small enough to not matter
                            // if things get synced again
                            // It could potentially only be a problem for people with super slow connections
                            // Or potentailly pay for data
                            // the sync time in the (local) DB will ALWAYS be UTC time. 
                            ret = Convert.ToInt32(rdr["last_sync"]);    // UTC time
                            // Also, if this clock is off by a few minutes then it would not sync either
                            // so lets take an arbitrary amount of 16 minutes to subtract from the time
                            ret = ret - (Properties.Settings.Default.TimeOffset * 60);
                            // TODO: in the fututre, query time.windows.com
                        }

                    }
                }
                conn.Close();
            }
            return ret;
        }

        public void UpdateSync(int timestamp = 0)
        {
            ExecuteNonQuery(Queries.UpdateLastSync(timestamp));
        }

        #endregion

        #region Terms 

        internal void ClearTerms(string lang_code)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.RemoveTerms(lang_code), conn))
                {
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        internal void SaveTerm(Term term)
        {
            int inserted = ExecuteNonQuery(Queries.InsertTerm(term));
            if(inserted < 1)
            {
                ExecuteNonQuery(Queries.UpdateTerm(term));
            }
        }

        public List<Term> GetTerms(string language, int parent)
        {
            List<Term> ret = new List<Term>();
            DataTable terms = ExecuteQuery(Queries.GetTerms(language, parent));
            if (terms.Rows.Count > 0)
            {
                foreach (DataRow r in terms.Rows)
                {
                    Term t = new Term();
                    t.LangCode = (string)r["lang_code"];
                    t.Tid = (int)r["tid"];
                    t.ParentTid = (int)r["parent"];
                    t.Name = (string)r["name"];
                    t.Description = (string)r["description"];
                    t.Alias = (string)r["alias"];
                    t.ImageData = (string)r["image"];
                    ret.Add(t);
                }
            }
            return ret;
        }

        public Term GetTerm(string language, int tid, int parent = 0)
        {
            Term t = new Term();
            DataTable terms = ExecuteQuery(Queries.GetTerm(language, tid));
            if (terms.Rows.Count == 1)
            {
                DataRow r = terms.Rows[0];
                t.LangCode = (string)r["lang_code"];
                t.Tid = (int)r["tid"];
                t.Name = (string)r["name"];
                t.Description = (string)r["description"];
                t.Alias = (string)r["alias"];
                t.ImageData = (string)r["image"];
            }
            return t;
        }

        public void RemoveTerms(string lang_code)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.RemoveTerms(lang_code), conn))
                {
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        #endregion

        #region Articles 

        public List<INode> GetArticles(string language, int tid, int limit = 0, bool favs_only = false, string terms = "")
        {
            List<INode> ret = new List<INode>();
            string query = string.Empty;
            if(favs_only)
            {
                query = Queries.GetFavArticles();
            }
            else
            {
                query = Queries.GetArticles(language, tid, limit);
            }
            if (!string.IsNullOrEmpty(terms))
            {
                query = Queries.SearchArticles(language, tid, limit, terms);
            }
            DataTable articles = ExecuteQuery(query);
            if (articles.Rows.Count > 0)
            {
                foreach (DataRow r in articles.Rows)
                {
                    Article a = new Article();
                    a.LangCode = (string)r["lang_code"];
                    a.Nid = (int)r["nid"];
                    a.Tid = (int)r["tid"];
                    a.Title = (string)r["title"];
                    a.Body = (string)r["body"];
                    a.ImageData = r.IsNull("image") ? "" : (string)r["image"];
                    a.Published = (int)r["published"];
                    a.Updated = (int)r["updated"];
                    a.Hash = (string)r["hash"];
                    a.Type = (string)r["type"];
                    a.IsFav = r["fav"] != DBNull.Value;
                    ret.Add(a);
                }
            }
            return ret;
        }

        public void RemoveArticles(int[] active, string lang_code)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            var parameters = new string[active.Length];
            for (int i = 0; i < active.Length; i++)
            {
                parameters[i] = string.Format("@p{0}", i);
                var ddd = string.Join(", ", parameters);
                cmd.Parameters.AddWithValue(parameters[i], active[i]);
                cmd.Parameters.AddWithValue("@lang", lang_code);
            }
            cmd.CommandText = string.Format("DELETE FROM articles WHERE lang_code = @lang AND nid NOT IN ({0})", string.Join(", ", parameters));
            ExecuteNonQueryCommand(cmd);
        }

        public void RemoveVideos(int[] active, string lang_code)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            var parameters = new string[active.Length];
            for (int i = 0; i < active.Length; i++)
            {
                parameters[i] = string.Format("@p{0}", i);
                var ddd = string.Join(", ", parameters);
                cmd.Parameters.AddWithValue(parameters[i], active[i]);
                cmd.Parameters.AddWithValue("@lang", lang_code);
            }
            cmd.CommandText = string.Format("DELETE FROM videos WHERE lang_code = @lang AND nid NOT IN ({0})", string.Join(", ", parameters));
            ExecuteNonQueryCommand(cmd);
        }

        public void RemoveGuidelines(int[] active, string lang_code)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            var parameters = new string[active.Length];
            for (int i = 0; i < active.Length; i++)
            {
                parameters[i] = string.Format("@p{0}", i);
                var ddd = string.Join(", ", parameters);
                cmd.Parameters.AddWithValue(parameters[i], active[i]);
                cmd.Parameters.AddWithValue("@lang", lang_code);
            }
            cmd.CommandText = string.Format("DELETE FROM guidelines WHERE lang_code = @lang AND nid NOT IN ({0})", string.Join(", ", parameters));
            ExecuteNonQueryCommand(cmd);
        }

        public void RemoveArticles(int tid, string lang_code)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.RemoveArticles(tid, lang_code), conn))
                {
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        public void SaveArticle(Article article)
        {
            int inserted = ExecuteNonQueryCommand(Queries.InsertArticleCommand(article));
            if(inserted < 1)
            {
                ExecuteNonQueryCommand(Queries.UpdateArticleCommand(article));
            }
        }

        #endregion

        #region Favourites

        public bool IsFav(string type, int nid)
        {
            try
            {
                DataTable fav = ExecuteQuery(Queries.GetFav(type, nid));
                return fav.Rows.Count > 0;
            }
            catch
            {
                return false;
            }
        }

        public bool AddOrDeleteFav(string type, int nid)
        {
            try
            {
                if (IsFav(type, nid))
                {
                    // It exists, so we must be unsetting it...so lets delete it!
                    ExecuteNonQuery(Queries.DeleteFav(type, nid));
                    return false;       // indicates fav no longer exists
                }
                else
                {
                    // It doesn't exist... so lets make it exist!
                    ExecuteNonQuery(Queries.InsertFav(type, nid));
                    return true;        // Indicates fav exists
                }
            }
            catch
            {
                return false;
            }
        }

        public int FavCount()
        {
            int ret = 0;
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.FavsCount(), conn))
                {
                    SQLiteDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            ret = Convert.ToInt32(rdr["fav_count"]);
                        }

                    }
                }
                conn.Close();
            }
            return ret;
        }

        #endregion

        #region New

        public List<INode> GetNew(string language)
        {
            List<INode> ret = new List<INode>();
            DataTable articles = ExecuteQuery(Queries.GetNew(language));
            if (articles.Rows.Count > 0)
            {
                foreach (DataRow r in articles.Rows)
                {
                    Node n = new Node();
                    n.LangCode = (string)r["lang_code"];
                    n.Nid = (int)r["nid"];
                    n.Tid = (int)r["tid"];
                    n.Title = (string)r["title"];
                    n.Body = (string)r["body"];
                    n.ImageData = r.IsNull("image") ? "" : (string)r["image"];
                    n.Published = (int)r["published"];
                    n.Updated = (int)r["updated"];
                    n.Hash = (string)r["hash"];
                    n.Type = (string)r["type"];
                    ret.Add(n);
                }
            }
            return ret;
        }

        public void SaveNew(Node node, SQLiteConnection conn = null)
        {
            ExecuteNonQuery(Queries.InsertNew(node.Type, node.Nid, node.LangCode));
        }

        #endregion

        #region Videos

        internal List<INode> GetVideos(string language, int tid = 0, bool favs_only = false, string terms = "")
        {
            List<INode> ret = new List<INode>();
            string query = Queries.Videos.Get(language, tid, favs_only);
            if (!string.IsNullOrEmpty(terms))
            {
                query = Queries.Videos.Search(language, tid, favs_only, terms);
            }
            DataTable videos = ExecuteQuery(query);
            if(videos.Rows.Count > 0)
            {
                foreach (DataRow r in videos.Rows)
                {
                    Video o = new Video();
                    o.LangCode = (string)r["lang_code"];
                    o.Nid = (int)r["nid"];
                    o.Tid = (int)r["tid"];
                    o.Title = (string)r["title"];
                    o.Body = (string)r["body"];
                    o.ImageData = r.IsNull("image") ? "" : (string)r["image"];
                    o.Published = (int)r["published"];
                    o.Updated = (int)r["updated"];
                    o.Hash = (string)r["hash"];
                    o.Sticky = (int)r["sticky"];
                    ret.Add(o);
                }
            }
            return ret;
        }

        public void SaveVideo(Video video)
        {
            int inserted = ExecuteNonQueryCommand(Queries.Videos.Insert(video));
            if(inserted < 1)
            {
                ExecuteNonQueryCommand(Queries.Videos.Update(video));
            }
        }

        internal Video GetVideo(int nid)
        {
            Video o = new Video();
            using (SQLiteConnection conn = new SQLiteConnection(ConnString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(Queries.Videos.Get(nid), conn))
                {
                    SQLiteDataReader r = cmd.ExecuteReader();
                    if (r.HasRows)
                    {
                        while (r.Read())
                        {
                            o.LangCode = (string)r["lang_code"];
                            o.Nid = (int)r["nid"];
                            o.Tid = (int)r["tid"];
                            o.Title = (string)r["title"];
                            o.Body = (string)r["body"];
                            o.ImageData = (string)r["image"];
                            o.Published = (int)r["published"];
                            o.Updated = (int)r["updated"];
                            o.Hash = (string)r["hash"];
                            o.Type = (string)r["type"];
                            o.Sticky = (int)r["sticky"];
                            o.VideoUrl = (string)r["video_url"];
                        }
                    }
                }
                conn.Close();
            }
            return o;
        }

        #endregion

        #region Guidelines

        internal List<INode> GetGuidelines(string language, int tid = 0, bool favs_only = false, string terms = "")
        {
            List<INode> ret = new List<INode>();
            string query = Queries.Guidelines.Get(language, tid, favs_only);
            if(!string.IsNullOrEmpty(terms))
            {
                query = Queries.Guidelines.Search(language, terms);
            }
            DataTable guides = ExecuteQuery(query);
            if (guides.Rows.Count > 0)
            {
                foreach (DataRow r in guides.Rows)
                {
                    Guideline o = new Guideline();
                    o.LangCode = (string)r["lang_code"];
                    o.Nid = (int)r["nid"];
                    o.Tid = (int)r["tid"];
                    o.Title = (string)r["title"];
                    o.Body = (string)r["body"];
                    o.ImageData = r.IsNull("image") ? "" : (string)r["image"];
                    o.Published = (int)r["published"];
                    o.Updated = (int)r["updated"];
                    o.GuideUrl = (string)r["guide_url"];
                    o.GuideTitle = (string)r["guide_title"];
                    o.Title = string.Format("{0}\n\n{1}", o.Title, o.GuideTitle);
                    o.GuideFile = !Convert.IsDBNull(r["guide_file"]) ? (byte[])r["guide_file"] : new byte[0];
                    ret.Add(o);
                }
            }
            return ret;
        }

        public void SaveGuideline(Guideline guide)
        {
            // First try to insert the node
            // If the row already exists, it will be silently ignored
            // If it has been inserted, it whoudl return a value of "1"
            int inserted = ExecuteNonQueryCommand(Queries.Guidelines.Insert(guide));
            // If the row has not been inserted, this means that it already exists and should be updated
            if(inserted < 1)
            {
                ExecuteNonQueryCommand(Queries.Guidelines.Update(guide));
            } 
        }
        #endregion
    }


}
