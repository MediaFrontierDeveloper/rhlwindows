﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RHL.lib.Schema;

namespace RHL.lib.Session
{
    class SessionManager
    {
        static readonly SessionManager _instance = new SessionManager();

        public static SessionManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public bool IsOnline { get; set; }
        public bool HasResynced { get; set; }

        public bool NewLoaded { get; set; }
        public bool GuidesLoaded { get; set; }
        public bool VideosLoaded { get; set; }

        public List<Term> MainTerms { get; set; }

        public int SelectedTerm { get; set; }
        public int PreviousTerm { get; set; }

        public void SetActiveTerm(int term)
        {
            PreviousTerm = SelectedTerm;
            SelectedTerm = term;
        }
    }
}
