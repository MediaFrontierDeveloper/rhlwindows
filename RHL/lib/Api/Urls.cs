﻿namespace RHL.lib.Api
{
    public class Urls
    {
        internal const string BASE_URL = "https://extranet.who.int/rhl/rhlapi2";
        
        internal static string Terms(string language, int timestamp)
        {
            return string.Format("{0}/{1}/terms/{2}/sync", BASE_URL, language, timestamp.ToString());
        }

        public static string Articles(string lang_code, int timestamp)
        {
            return string.Format("{0}/{1}/articles/{2}/sync", BASE_URL, lang_code, timestamp.ToString());
        }

        public static string ArticlesActive(string lang_code)
        {
            return string.Format("{0}/{1}/articles/active", BASE_URL, lang_code);
        }

        public static string Videos(string lang_code, int timestamp)
        {
            return string.Format("{0}/{1}/videos/{2}/sync", BASE_URL, lang_code, timestamp.ToString());
        }

        public static string VideosActive(string lang_code)
        {
            return string.Format("{0}/{1}/videos/active", BASE_URL, lang_code);
        }

        public static string Guidelines(string lang_code, int timestamp)
        {
            return string.Format("{0}/{1}/guidelines/{2}/sync", BASE_URL, lang_code, timestamp.ToString());
        }

        public static string GuidelinesActive(string lang_code)
        {
            return string.Format("{0}/{1}/videos/active", BASE_URL, lang_code);
        }

        public static string New(string lang_code)
        {
            return string.Format("{0}/{1}/new", BASE_URL, lang_code);
        }
    }
}