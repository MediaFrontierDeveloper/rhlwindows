﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace RHL.lib.Api
{
    public class ApiManager
    {

        static readonly ApiManager _instance = new ApiManager();

        public static ApiManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public string GET(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }

        public bool HasConnection()
        {
            WebClient client = new WebClient();
            try
            {
                // This is a vey fast api call to check to see if the RHL server is up
                // This is the most efficent way of testing both and internet connection
                // is available and the RHL api server is responding to requests
                // This is a sync time in the future to just return a very small 200 response
                using (client.OpenRead("http://46.101.209.4/rhlapi2/en/terms/9999999999999/sync")) { }
                return true;
            }
            catch (WebException)
            {
                return false;
            }
        }
    }
}
