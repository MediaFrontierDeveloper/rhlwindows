﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Net;
using RHL.lib.Extensions;

namespace RHL.lib.Schema
{
    public class Guideline : Node
    {
        [JsonProperty("guideUrl")]
        public string GuideUrl { get; set; }

        [JsonProperty("guideTitle")]
        public string GuideTitle { get; set; }

        public byte[] GuideFile { get; set; }

        public string GuideFilePath
        {
            get
            {
                if(GuideFile.Length > 0)
                {
                    string path = Path.Combine(Path.GetTempPath(), Filename);
                    if(!File.Exists(path))
                    {
                        File.WriteAllBytes(path, GuideFile);
                    }
                    return path;
                }
                return null;
            }
        }

        public string Filename
        {
            get
            {
                MD5 md5 = MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(GuideUrl);
                byte[] hash = md5.ComputeHash(inputBytes);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return Path.ChangeExtension(sb.ToString(), "pdf");
            }
        }

        public byte[] DownloadGuide()
        {
            string filePath = Path.GetTempFileName();
            using (WebClient wc = new WebClient())
            {
                try
                {
                    wc.DownloadFile(new System.Uri(GuideUrl), filePath);
                    return File.ReadAllBytes(filePath);
                }
                catch
                {
                    //throw;
                    // Usually means that the URL is not available
                    // Or can't be retrived due to authorization or redirect issues
                    // Ignore!!
                    // just return an empty byte array
                    return new byte[0];
                }
            }
        }
    }
}
