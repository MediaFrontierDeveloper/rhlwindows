﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RHL.lib.Schema
{
    public interface INode
    {
        int Nid { get; set; }
        int Tid { get; set; }
        string LangCode { get; set; }
        string Title { get; set; }
        int Published { get; set; }
        int Updated { get; set; }
        string Body { get; set; }
        string ImageData { get; set; }
        string Hash { get; set; }
        string Type { get; set; }
        Image ThumbNail { get;  }
        string CacheKey { get; }
        string DisplayDate { get; }
        bool IsFav { get; set; }
        int Sticky { get; set; }
    }
}
