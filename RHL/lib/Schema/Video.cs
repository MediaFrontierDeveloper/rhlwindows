﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RHL.lib.Schema
{
    public class Video : Node
    {
        [JsonProperty("videoUrl")]
        public string VideoUrl { get; set; }
    }
}
