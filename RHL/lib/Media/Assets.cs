﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Reflection;
using System.IO;

namespace RHL.lib.Media
{
    public static class Assets
    {
        public static Image GetImage(string asset, int width = 0, int height = 0)
        {
            Assembly rhl;
            rhl = Assembly.GetExecutingAssembly();
            Stream file = rhl.GetManifestResourceStream(string.Format("RHL.assets.{0}", asset));
            if(width != 0 && height != 0)
            {
                return ImageManager.FixedSize(Image.FromStream(file), width, height, true);
            }
            return Image.FromStream(file);
        }

        public static Stream ResourceFile(string asset, string path = "")
        {
            Assembly rhl;
            string resourcePath = string.Format("RHL.Resources.{0}", asset);
            if (!string.IsNullOrEmpty(path))
            {
                resourcePath = string.Format("{0}", path);
            }
            rhl = Assembly.GetExecutingAssembly();
            Stream file = rhl.GetManifestResourceStream(resourcePath);
            return file;
        }

        public static string[] GetAllFiles(string path, string extension)
        {
            var executingAssembly = Assembly.GetExecutingAssembly();
            string folderName = string.Format("{0}.Resources.{1}", executingAssembly.GetName().Name, path);
            return executingAssembly
                .GetManifestResourceNames()
                .Where(r => r.StartsWith(folderName) && r.EndsWith(extension))
                //.Select(r => r.Substring(constantResName.Length + 1))
                .ToArray();
        }
    }
}

