﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RHL.lib.Media
{
    public class ColorManager
    {
        public static Color BrandColor 
        {
            get
            {
                return ColorTranslator.FromHtml(Properties.Settings.Default.BrandColor);
            }
        }
    }
}
