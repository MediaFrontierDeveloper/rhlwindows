﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RHL.lib.Extensions
{
    public static class UIExtensions
    {
        public static ToolStripMenuItem CheckSingleValue(this ToolStripMenuItem menu, ToolStripItem itemToCheck)
        {
            if(menu.HasDropDownItems)
            {
                foreach(ToolStripMenuItem it in menu.DropDownItems)
                {
                    it.Checked = (it.Text == itemToCheck.Text);
                }
            }
            return menu;
        }
    }
}
