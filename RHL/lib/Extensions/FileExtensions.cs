﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RHL.lib.Extensions
{
    public static class FileExtensions
    {
        public static bool ExistsIgnoreExtension(string path, string fileName)
        {
            DirectoryInfo root = new DirectoryInfo(path);
            FileInfo[] listfiles = root.GetFiles(string.Format("{0}.*", fileName));
            return listfiles.Length > 0;
        }
    }
}
