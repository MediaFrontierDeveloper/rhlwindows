﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RHL.assets
{
    public static class Template
    {
        public static string RhlHtmlDocument(string body)
        {
            string template = @"
<html>
<head>
<style>
html, body { font-family: arial, sans-serif; font-size: 12pt;}
h1,h2,h3,h4, h5 { color: #2a6dae; font-weight: bold; }
</style>
<body>" + body + "</body></html>";
            return template;
        }


        public static string YouTubePlayer(string src)
        {
            string template = @"
<html>
<head>
<style>
html, body {background-color:#000;}
p {text-align: center;}
</style>
<body><p><iframe width=""853"" height=""480"" src=""" + src + @"?autoplay=1&autohide=1"" frameborder=""0""></iframe></p>";
            return template;
        }
    }
}
