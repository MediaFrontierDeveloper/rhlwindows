﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using RHL.lib.Db;
using RHL.lib.Sync;
using RHL.lib.Schema;
using RHL.UserControls;
using RHL.lib.Media;
using RHL.assets;
using RHL.lib.Session;
using System.Drawing;
using RHL.lib.Text;
using RHL.lib.Cache;
using System.Threading.Tasks;
using System.Threading;

namespace RHL
{
    public partial class RHLApp2 : Form
    {
        DbManager db = DbManager.Instance;
        SyncManager sm = SyncManager.Instance;
        SessionManager ss = SessionManager.Instance;
        public ResourceManager rm;
        public CultureInfo culture;

        public RHLApp2()
        {
            InitializeComponent();
            rm = new ResourceManager("RHL.Resources.strings", Assembly.Load("RHL"));
            culture = CultureInfo.CreateSpecificCulture(Properties.Settings.Default.Language);
            SetFormState(FormStates.Default);
            SetInitialLanguage();
            UpdateFavsCount();
            ss.NewLoaded = false;
            ss.GuidesLoaded = false;
            ss.VideosLoaded = false;
            ss.SelectedTerm = 0;
            page_About1.SetText(culture);
        }

        #region Form States 

        public enum FormStates
        {
            Default,
            ArticleDetail,
            TermDetail,
            Guidelines,
            Videos,
            Favs,
            Search,
            About
        }

        public void SetLabels()
        {
            uxMenu_Language.Text = rm.GetString("settings_language", culture);
            uxTabs_About.Text = rm.GetString("action_about", culture);
            uxTabs_Library.Text = rm.GetString("library_title", culture);
            uxTabs_Videos.Text = rm.GetString("videos_title", culture);
            uxTabs_Guidelines.Text = rm.GetString("guidelines_title", culture);
            uxTabs_Search.Text = rm.GetString("action_search", culture);
            uxSearch_Submit.Text = rm.GetString("action_search", culture);
            this.Text = rm.GetString("title_rhl", culture);
            uxTermsTitle.Text = rm.GetString("contents", culture);
            UpdateFavsCount();
        }

        public void SetFormState(FormStates state)
        {
            Cursor.Current = Cursors.WaitCursor;
            SetLabels();
            switch (state)
            {
                case FormStates.ArticleDetail:
                    uxTabs.SelectedTab = uxTabs_Library;
                    uxNodeTitle.Visible = false;
                    uxNodes.Visible = false;
                    uxTermsTitle.Visible = false;
                    uxTerms.Visible = false;
                    uxArticleDetail.Visible = true;
                    uxTitleBar.ActionVisible = true;
                    uxTermHeader.Visible = false;
                    uxVideosTitle.Visible = false;
                    uxVideos.Visible = false;
                    uxGuidelinesTitle.Visible = false;
                    uxGuidelines.Visible = false;
                    uxFavs_Library_Title.Visible = false;
                    uxFavs_Library.Visible = false;
                    uxFavs_Video_Title.Visible = false;
                    uxFavs_Video.Visible = false;
                    break;

                case FormStates.TermDetail:
                    uxNodeTitle.Visible = uxNodes.Controls.Count > 0;
                    uxNodes.Visible = uxNodes.Controls.Count > 0;
                    uxTermsTitle.Visible = uxTerms.Controls.Count > 0;
                    uxTerms.Visible = uxTerms.Controls.Count > 0;
                    uxArticleDetail.Visible = false;
                    uxTitleBar.ActionVisible = true;
                    uxTermHeader.Visible = true;
                    uxVideosTitle.Visible = false;
                    uxVideos.Visible = false;
                    uxGuidelinesTitle.Visible = false;
                    uxGuidelines.Visible = false;
                    uxFavs_Library_Title.Visible = false;
                    uxFavs_Library.Visible = false;
                    uxFavs_Video_Title.Visible = false;
                    uxFavs_Video.Visible = false;
                    break;

                case FormStates.Videos:
                    uxNodeTitle.Visible = false;
                    uxNodes.Visible = false;
                    uxTermsTitle.Visible = false;
                    uxTerms.Visible = false;
                    uxArticleDetail.Visible = false;
                    uxTitleBar.ActionVisible = true;
                    uxTermHeader.Visible = false;
                    uxVideosTitle.Visible = true;
                    uxVideos.Visible = true;
                    uxGuidelinesTitle.Visible = false;
                    uxGuidelines.Visible = false;
                    uxFavs_Library_Title.Visible = false;
                    uxFavs_Library.Visible = false;
                    uxFavs_Video_Title.Visible = false;
                    uxFavs_Video.Visible = false;
                    uxTabs.SelectedTab = uxTabs_Videos;
                    uxTitleBar.ClearBar();
                    LoadVideos();
                    break;

                case FormStates.Guidelines:
                    uxNodeTitle.Visible = false;
                    uxNodes.Visible = false;
                    uxTermsTitle.Visible = false;
                    uxTerms.Visible = false;
                    uxArticleDetail.Visible = false;
                    uxTitleBar.ActionVisible = true;
                    uxTermHeader.Visible = false;
                    uxVideosTitle.Visible = false;
                    uxVideos.Visible = false;
                    uxFavs_Library_Title.Visible = false;
                    uxFavs_Library.Visible = false;
                    uxFavs_Video_Title.Visible = false;
                    uxFavs_Video.Visible = false;
                    uxFavs_Guidelines.Visible = false;
                    uxGuidelinesTitle.Visible = true;
                    uxGuidelines.Visible = true;
                    uxTabs.SelectedTab = uxTabs_Guidelines;
                    uxTitleBar.ClearBar();
                    LoadGuidelines();
                    break;

                case FormStates.Favs:
                    uxNodeTitle.Visible = false;
                    uxNodes.Visible = false;
                    uxTermsTitle.Visible = false;
                    uxTerms.Visible = false;
                    uxArticleDetail.Visible = false;
                    uxTitleBar.ActionVisible = true;
                    uxTermHeader.Visible = false;
                    uxVideosTitle.Visible = false;
                    uxVideos.Visible = false;
                    uxGuidelinesTitle.Visible = false;
                    uxGuidelines.Visible = false;
                    uxFavs_Library_Title.Visible = uxFavs_Library.Controls.Count > 0;
                    uxFavs_Library.Visible = true;
                    uxFavs_Video_Title.Visible = uxFavs_Video.Controls.Count > 0;
                    uxFavs_Video.Visible = true;
                    uxFavs_Guidelines_Title.Visible = uxFavs_Guidelines.Controls.Count > 0;
                    uxFavs_Guidelines.Visible = true;
                    UpdateFavsCount(uxFavs_Library.Controls.Count + uxFavs_Video.Controls.Count);
                    uxTabs.SelectedTab = uxTabs_Favs;
                    uxTitleBar.ClearBar();
                    break;


                case FormStates.Search:
                    uxNodeTitle.Visible = false;
                    uxNodes.Visible = false;
                    uxTermsTitle.Visible = false;
                    uxTerms.Visible = false;
                    uxArticleDetail.Visible = false;
                    uxTitleBar.ActionVisible = true;
                    uxTermHeader.Visible = false;
                    uxVideosTitle.Visible = false;
                    uxVideos.Visible = false;
                    uxGuidelinesTitle.Visible = false;
                    uxGuidelines.Visible = false;
                    uxFavs_Library_Title.Visible = false;
                    uxFavs_Library.Visible = false;
                    uxFavs_Video_Title.Visible = false;
                    uxFavs_Video.Visible = false;
                    uxTabs.SelectedTab = uxTabs_Search;
                    uxTitleBar.ClearBar();
                    break;

                case FormStates.About:
                    uxNodeTitle.Visible = false;
                    uxNodes.Visible = false;
                    uxTermsTitle.Visible = false;
                    uxTerms.Visible = false;
                    uxArticleDetail.Visible = false;
                    uxTitleBar.ActionVisible = true;
                    uxTermHeader.Visible = false;
                    uxVideosTitle.Visible = false;
                    uxVideos.Visible = false;
                    uxGuidelinesTitle.Visible = false;
                    uxGuidelines.Visible = false;
                    uxFavs_Library_Title.Visible = false;
                    uxFavs_Library.Visible = false;
                    uxFavs_Video_Title.Visible = false;
                    uxFavs_Video.Visible = false;
                    uxTabs.SelectedTab = uxTabs_About;
                    uxTitleBar.ClearBar();
                    break;

                default:
                    // Default loaded
                    LoadNewInRhl();
                    LoadTerms();
                    uxTitleBar.Title = string.Empty;
                    uxNodeTitle.Visible = true;
                    uxNodes.Visible = true;
                    uxTermsTitle.Visible = true;
                    uxTerms.Visible = true;
                    uxArticleDetail.Visible = false;
                    uxTermHeader.Visible = false;
                    uxTitleBar.ActionVisible = false;
                    uxVideosTitle.Visible = false;
                    uxVideos.Visible = false;
                    uxFavs_Library_Title.Visible = false;
                    uxFavs_Library.Visible = false;
                    uxFavs_Video_Title.Visible = false;
                    uxFavs_Video.Visible = false;
                    uxTabs.SelectedTab = uxTabs_Library;

                  

                    // If we are working offline put a message in the titlebar
                    if(!ss.IsOnline)
                    {
                        this.Text = string.Format("{0} (offline mode)", this.Text);
                    }
                    break;
                    
            }
            Cursor.Current = Cursors.Default;
        }

        #endregion

        #region Db Loaders

        #region Front page new

        internal void LoadNewInRhl()
        {
            uxNodes.Controls.Clear();
            List<INode> nodes = db.GetNew(Properties.Settings.Default.Language);
            foreach (INode node in nodes)
            {
                Node_Teaser nt = new Node_Teaser(node);
                uxNodes.Controls.Add(nt);
                nt.NodeClick += LoadArticleDetail;
            }
            uxNodeTitle.Text = rm.GetString("new_in_rhl", culture);
            ss.NewLoaded = true;
        }

        #endregion

        #region Library Node / term loaders

        internal void LoadTerms(int tid = 0)
        {
            uxTerms.Controls.Clear();
            List<Term> terms = db.GetTerms(Properties.Settings.Default.Language, tid);
            foreach (Term term in terms)
            {
                // Add to row 
                uxTerms.RowCount = uxTerms.RowCount + 2;
                uxTerms.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                // check to see if this teaser is in the cache already...
                Term_Teaser tt;
                tt = CacheManager.Get<Term_Teaser>(term.CacheKey());
                if (tt == null)
                {
                    tt = new Term_Teaser(term);
                }
                tt.TermClick += Term_Teaser;
                uxTerms.Controls.Add(tt, 0, uxTerms.RowCount - 1);
            }
        }

        internal void LoadLibraryNodes(List<INode> nodes)
        {
            uxNodes.Controls.Clear();
            foreach (INode node in nodes)
            {
                Node_Teaser nt = new Node_Teaser(node);
                uxNodes.Controls.Add(nt);
                nt.NodeClick += LoadArticleDetail;
            }
        }


        #endregion

        #region Term Detail 

        /// <summary>
        /// Load a term detail page including it's articles and sub terms
        /// </summary>
        /// <param name="e"></param>
        internal void LoadTermDetail(Term e)
        {
            uxTermHeader_Description.Text = Parser.ScrubHtml(e.Description);
            if (e.ThumbNail != null)
            {
                uxTermHeader_Image.Image = ImageManager.FixedSize(e.ThumbNail, 665, 305, false);
            }
            else
            {
                uxTermHeader_Image.Visible = false;
            }
            uxTitleBar.Title = e.Name;
            uxTermsTitle.Text = uxNodeTitle.Text = rm.GetString("subsections", culture);
            LoadTerms(e.Tid);
            uxNodeTitle.Text = rm.GetString("library_upcase", culture);
            LoadLibraryNodes(db.GetArticles(Properties.Settings.Default.Language, e.Tid));
            SetFormState(FormStates.TermDetail);
            ss.PreviousTerm = e.ParentTid;
        }

        #endregion

        #region Article Detail Page

        internal void LoadArticleDetail(object sender, INode e)
        {
            if (e != null)
            {
                ss.SetActiveTerm(e.Tid);
                uxArticleDetail_Title.Text = e.Title;
                uxArticleDetail.Tag = e.Tid;
                uxArticleDetail_Date.Text = e.DisplayDate;
                if (e.ThumbNail != null)
                {
                    uxArticleDetail_Image.Image = ImageManager.FixedSize(e.ThumbNail, 400, 360, false);
                }
                else
                {
                    uxArticleDetail_Image.Visible = false;
                }
                uxArticleDetail_Body.DocumentText = Template.RhlHtmlDocument(e.Body);
                uxArticleDetail_Body.DocumentCompleted += (send, evnt) =>
                {
                    Size browserSize = uxArticleDetail_Body.Document.Body.ScrollRectangle.Size;
                    browserSize.Height = browserSize.Height + 300;
                    uxArticleDetail_Body.Size = browserSize;
                };

                // Title bar
                uxTitleBar.Title = db.GetTerm(Properties.Settings.Default.Language, e.Tid).Name;

                // Form UI
                this.Text = string.Format("RHL Article: {0}", e.Title);
                uxFav.LoadFav(e.Type, e.Nid);
                SetFormState(FormStates.ArticleDetail);
            }
        }

        #endregion

        #region Videos

        internal void LoadVideos()
        {
            if(!ss.VideosLoaded)
            {
                uxVideosTitle.Text = "VIDEOS";
                uxVideos.Controls.Clear();
                List<INode> nodes = db.GetVideos(Properties.Settings.Default.Language);
                foreach (INode node in nodes)
                {
                    Node_Teaser nt = new Node_Teaser(node);
                    uxVideos.Controls.Add(nt);
                    nt.NodeClick += Load_Video_Player;
                }
               // uxTitleBar.ActionClick += UxTitleBar_Videos_ActionClick;
                ss.VideosLoaded = true;
            }
        }

        private void Load_Video_Player(object sender, INode e)
        {
            Player player = new Player();
            player.Text = string.Format("Now Playing: {0}", e.Title);
            player.LoadVideo(e.Nid);
            player.ShowDialog();
        }

        #endregion

        #region Guidelines

        internal void LoadGuidelines()
        {
            if(!ss.GuidesLoaded)
            {
                uxGuidelinesTitle.Text = "GUIDELINES";
                uxGuidelines.Controls.Clear();
                List<INode> nodes = db.GetGuidelines(Properties.Settings.Default.Language);
                foreach (INode node in nodes)
                {
                    Node_Teaser nt = new Node_Teaser(node);
                    uxGuidelines.Controls.Add(nt);
                    nt.NodeClick += Open_Guideline;
                }
               // uxTitleBar.ActionClick += UxTitleBar_Guidelines_ActionClick;
                ss.GuidesLoaded = true;
            }
        }

        private void Open_Guideline(object sender, INode e)
        {
            Guideline guide = (Guideline)e;
            if(guide.GuideFile.Length > 0)
            {
                if(guide.GuideFilePath != null)
                {
                    Process.Start(guide.GuideFilePath);
                }
                else
                {
                    MessageBox.Show("There was an error downloading this guideline.", "Could not download Guideline", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("There was an error downloading this guideline.", "Could not download Guideline", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #region Favs

        internal void LoadFavs()
        {
            uxFavs_Library_Title.Text = rm.GetString("favourites_title", culture);
            uxFavs_Library.Controls.Clear();
            List<INode> nodes = db.GetArticles(Properties.Settings.Default.Language, 0, 0, true);
            foreach (INode node in nodes)
            {
                Node_Teaser nt = new Node_Teaser(node);
                uxFavs_Library.Controls.Add(nt);
                nt.NodeClick += LoadArticleDetail;
            }

            uxFavs_Video_Title.Text = rm.GetString("videos_upcase", culture);
            uxFavs_Video.Controls.Clear();
            nodes = db.GetVideos(Properties.Settings.Default.Language, 0, true);
            foreach (INode node in nodes)
            {
                Node_Teaser video = new Node_Teaser(node);
                uxFavs_Video.Controls.Add(video);
                video.NodeClick += Load_Video_Player;
            }

            //uxTitleBar.ActionClick += UxTitleBar_Favs_ActionClick;
            SetFormState(FormStates.Favs);
        }

        public void UpdateFavsCount(int favsCount = -1)
        {
            if (favsCount < 1)
            {
                favsCount = db.FavCount();
            }
            if (favsCount > 0)
            {
                uxTabs_Favs.Text = string.Format("{0} ({1})", rm.GetString("favourites_title", culture), favsCount);
                uxTabs_Favs.Refresh();
            }
            if(favsCount == 0)
            {
                uxTabs_Favs.Text = string.Format("{0}", rm.GetString("favourites_title", culture));
                uxTabs_Favs.Refresh();
            }
        }

        #endregion

        #region Search 

        private void uxSearch_Submit_Click(object sender, EventArgs e)
        {
            uxSearch_Submit.Text = rm.GetString("action_search", culture) + "...";
            uxSearch_Message.Text = rm.GetString("action_search", culture) + "...";
            uxSearch_Message.Visible = true;
            uxSearch_Message.Invalidate();
            uxSearch_Message.Update();
            uxSearch_Message.Refresh();
            Application.DoEvents();
            DoSearch();
        }

        private void DoSearch()
        {
            if (uxSearch_Terms.TextLength < 3)
            {
                MessageBox.Show("Please enter search terms of at least 3 characters");
                uxSearch_Submit.Text = rm.GetString("action_search", culture);
                uxSearch_Message.Text = "";
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;
                bool noResults = true;
                // Do a search!
                // Articles
                uxSearch_Library.Controls.Clear();
                List<INode> nodes = db.GetArticles(Properties.Settings.Default.Language, 0, 0, false, uxSearch_Terms.Text.ToLower());
                if (nodes.Count > 0)
                {
                    foreach (INode node in nodes)
                    {
                        Node_Teaser nt = new Node_Teaser(node);
                        uxSearch_Library.Controls.Add(nt);
                        nt.NodeClick += LoadArticleDetail;
                    }
                    uxSearch_Library_Title.Text = string.Format("{0} ({1})", rm.GetString("library_upcase", culture), nodes.Count);
                    uxSearch_Library.Visible = true;
                    uxSearch_Library_Title.Visible = true;
                    noResults = false;
                }
                // Videos
                uxSearch_Videos.Controls.Clear();
                nodes = db.GetVideos(Properties.Settings.Default.Language, 0, false, uxSearch_Terms.Text.ToLower());
                if (nodes.Count > 0)
                {
                    foreach (INode node in nodes)
                    {
                        Node_Teaser nt = new Node_Teaser(node);
                        uxSearch_Videos.Controls.Add(nt);
                        nt.NodeClick += Load_Video_Player;
                    }
                    uxSearch_Videos_Title.Text = string.Format("{0} ({1})", rm.GetString("videos_upcase", culture), nodes.Count);
                    uxSearch_Videos.Visible = true;
                    uxSearch_Videos_Title.Visible = true;
                    noResults = false;
                }

                // Guidelines
                uxSearch_Guidelines.Controls.Clear();
                nodes = db.GetGuidelines(Properties.Settings.Default.Language, 0, false, uxSearch_Terms.Text.ToLower());
                if (nodes.Count > 0)
                {
                    foreach (INode node in nodes)
                    {
                        Node_Teaser nt = new Node_Teaser(node);
                        uxSearch_Guidelines.Controls.Add(nt);
                        nt.NodeClick += Open_Guideline;
                    }
                    uxSearch_Guidelines_Title.Text = string.Format("{0} ({1}))", rm.GetString("guidelines_upcase", culture), nodes.Count);
                    uxSearch_Guidelines.Visible = true;
                    uxSearch_Guidelines_Title.Visible = true;
                    noResults = false;
                }
                uxSearch_Submit.Text = rm.GetString("action_search", culture);
                if (noResults)
                {
                    uxSearch_Message.Text = string.Format("Sorry no items matched your search of '{0}'", uxSearch_Terms.Text);
                    uxSearch_Message.Visible = true;
                    uxSearch_Guidelines_Title.Text = "";
                    uxSearch_Guidelines.Visible = false;
                    uxSearch_Videos_Title.Text = "";
                    uxSearch_Videos.Visible = false;
                    uxSearch_Library_Title.Text = "";
                    uxSearch_Library.Visible = false;
                    uxSearch_Guidelines_Title.Visible = true;
                    uxSearch_Terms.Text = "";
                }
                else
                {
                    uxSearch_Message.Text = "";
                    uxSearch_Message.Visible = false;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Title bar event handlers

        private void UxTitleBar_TermTeaser_ActionClick(object sender, EventArgs e)
        {
            SetFormState(FormStates.Default);
        }

        private void UxTitleBar_Videos_ActionClick(object sender, EventArgs e)
        {
            SetFormState(FormStates.Default);
        }

        private void UxTitleBar_Guidelines_ActionClick(object sender, EventArgs e)
        {
            SetFormState(FormStates.Default);
        }
    
        private void UxTitleBar_Favs_ActionClick(object sender, EventArgs e)
        {
            SetFormState(FormStates.Default);
        }

        #endregion

        #region Event Handlers

        private void Term_Teaser(object sender, Term e)
        {
            LoadTermDetail(e);
        }

        #endregion

        #region Menu Events

        private void uxMenuLanguage_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Tag != null)
                    SwitchLanguage(e.ClickedItem.Tag.ToString());
            }
            catch
            {
                throw;
                // TODO: Exception handling
            }
        }

        private void uxMenu_Settings_Sync_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Sync sync = new Sync();
            sync.DoSync();
            SetFormState(FormStates.Default);
            Cursor.Current = Cursors.Default;   
        }

        #endregion

        #region Tabs

        private void uxTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (uxTabs.SelectedTab == uxTabs_Library)
            {
                SetFormState(FormStates.Default);
            }
            if (uxTabs.SelectedTab == uxTabs_Videos)
            {
                SetFormState(FormStates.Videos);
            }
            if (uxTabs.SelectedTab == uxTabs_Guidelines)
            {
                SetFormState(FormStates.Guidelines);
            }
            if (uxTabs.SelectedTab == uxTabs_Favs)
            {
                LoadFavs();
                SetFormState(FormStates.Favs);
            }
            if (uxTabs.SelectedTab == uxTabs_About)
            {
                SetFormState(FormStates.About);
            }
            if (uxTabs.SelectedTab == uxTabs_Search)
            {
                uxSearch_Submit.Text = rm.GetString("action_search", culture);
                uxSearch_Message.Text = "";
                uxSearch_Message.Visible = false;
            }
        }

        private void uxTabs_Click(object sender, EventArgs e)
        {
            if (uxTabs.SelectedTab == uxTabs_Library)
            {
                SetFormState(FormStates.Default);
            }
        }

        #endregion

        #region Language

        internal void CheckLang()
        {
            foreach (ToolStripMenuItem item in uxMenu_Language.DropDownItems)
            {
                item.Checked = (item.Tag.ToString() == Properties.Settings.Default.Language);
            }
        }

        internal void SwitchLanguage(string lang_code)
        {
            Cursor.Current = Cursors.WaitCursor;
            Properties.Settings.Default.Language = lang_code;
            Properties.Settings.Default.Save();
            CheckLang();
            culture = CultureInfo.CreateSpecificCulture(Properties.Settings.Default.Language);
            ss.NewLoaded = false;
            ss.GuidesLoaded = false;
            ss.VideosLoaded = false;
            Task.Factory.StartNew(() => LoadGuidelines());
            Task.Factory.StartNew(() => LoadVideos());
            SetFormState(FormStates.Default);
            page_About1.SetText(culture);
            uxTabs.Refresh();
            uxTabs_About.Refresh();
            uxTabs_Favs.Refresh();
            uxTabs_Library.Refresh();
            uxTabs_Search.Refresh();
            uxTabs_Videos.Refresh();
            this.Refresh();
            Cursor.Current = Cursors.Default;
        }

        private void SetInitialLanguage()
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.Language))
            {
                foreach (ToolStripMenuItem item in uxMenu_Language.DropDownItems)
                {
                    item.Checked = (item.Tag.ToString() == Properties.Settings.Default.Language);
                }
            }
        }

        #endregion

        #region Graphics 

        /// <summary>
        /// Add double buffering to stop control flicker
        /// </summary>
        /// 
        int originalExStyle = -1;
        bool enableFormLevelDoubleBuffering = true;

        protected override CreateParams CreateParams
        {
            get
            {
                if (originalExStyle == -1)
                    originalExStyle = base.CreateParams.ExStyle;

                CreateParams cp = base.CreateParams;
                if (enableFormLevelDoubleBuffering)
                    cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                else
                    cp.ExStyle = originalExStyle;

                return cp;
            }
        }

        public void TurnOffFormLevelDoubleBuffering()
        {
            enableFormLevelDoubleBuffering = false;
            this.MaximizeBox = true;
        }

        public void TurnOnFormLevelDoubleBuffering()
        {
            enableFormLevelDoubleBuffering = true;
            this.MaximizeBox = true;
        }

        private void RHLApp2_ResizeEnd(object sender, EventArgs e)
        {
            TurnOnFormLevelDoubleBuffering();
        }

        private void RHLApp2_ResizeBegin(object sender, EventArgs e)
        {
            TurnOffFormLevelDoubleBuffering();
        }

        #endregion

        private void uxMenuDocument_Print_Click(object sender, EventArgs e)
        {
            
        }
    }
}