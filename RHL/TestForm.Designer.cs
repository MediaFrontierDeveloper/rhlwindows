﻿namespace RHL
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.title_Bar21 = new RHL.UserControls.Title_Bar2();
            this.SuspendLayout();
            // 
            // title_Bar21
            // 
            this.title_Bar21.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.title_Bar21.Location = new System.Drawing.Point(12, 0);
            this.title_Bar21.Name = "title_Bar21";
            this.title_Bar21.Size = new System.Drawing.Size(1057, 59);
            this.title_Bar21.TabIndex = 0;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 495);
            this.Controls.Add(this.title_Bar21);
            this.Name = "TestForm";
            this.Text = "TestForm";
            this.ResumeLayout(false);

        }

        #endregion

        private UserControls.Title_Bar2 title_Bar21;
    }
}