﻿using RHL.lib.Media;

namespace RHL
{
    partial class RHLApp2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
   #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RHLApp2));
            this.uxMenu = new System.Windows.Forms.MenuStrip();
            this.uxMenu_Language = new System.Windows.Forms.ToolStripMenuItem();
            this.uxMenuLanguage_En = new System.Windows.Forms.ToolStripMenuItem();
            this.uxMenuLanguage_ZH_HANS = new System.Windows.Forms.ToolStripMenuItem();
            this.uxMenuLanguage_FR = new System.Windows.Forms.ToolStripMenuItem();
            this.uxMenuLanguage_ES = new System.Windows.Forms.ToolStripMenuItem();
            this.uxMenuLanguage_PT_BR = new System.Windows.Forms.ToolStripMenuItem();
            this.uxMenu_Document = new System.Windows.Forms.ToolStripMenuItem();
            this.uxMenuDocument_Print = new System.Windows.Forms.ToolStripMenuItem();
            this.uxMenuLanguage_RU = new System.Windows.Forms.ToolStripMenuItem();
            this.uxTabs = new System.Windows.Forms.TabControl();
            this.uxTabs_Library = new System.Windows.Forms.TabPage();
            this.uxPanel_Library = new System.Windows.Forms.FlowLayoutPanel();
            this.uxTermHeader = new System.Windows.Forms.TableLayoutPanel();
            this.uxTermHeader_Image = new System.Windows.Forms.PictureBox();
            this.uxTermHeader_PanelTitle = new System.Windows.Forms.FlowLayoutPanel();
            this.uxTermHeader_Description = new System.Windows.Forms.Label();
            this.uxNodeTitle = new System.Windows.Forms.Label();
            this.uxNodes = new System.Windows.Forms.FlowLayoutPanel();
            this.uxTermsTitle = new System.Windows.Forms.Label();
            this.uxTerms = new System.Windows.Forms.TableLayoutPanel();
            this.uxArticleDetail = new System.Windows.Forms.TableLayoutPanel();
            this.uxArticleDetail_Image = new System.Windows.Forms.PictureBox();
            this.uxArticleDetail_PanelTitle = new System.Windows.Forms.FlowLayoutPanel();
            this.uxArticleDetail_Title = new System.Windows.Forms.Label();
            this.uxArticleDetail_Date = new System.Windows.Forms.Label();
            this.uxArticleDetail_Body = new System.Windows.Forms.WebBrowser();
            this.uxTabs_Guidelines = new System.Windows.Forms.TabPage();
            this.uxPanel_Guidelines = new System.Windows.Forms.FlowLayoutPanel();
            this.uxGuidelinesTitle = new System.Windows.Forms.Label();
            this.uxGuidelines = new System.Windows.Forms.FlowLayoutPanel();
            this.uxTabs_Videos = new System.Windows.Forms.TabPage();
            this.uxPanel_Video = new System.Windows.Forms.FlowLayoutPanel();
            this.uxVideosTitle = new System.Windows.Forms.Label();
            this.uxVideos = new System.Windows.Forms.FlowLayoutPanel();
            this.uxTabs_Favs = new System.Windows.Forms.TabPage();
            this.uxPanel_Favs = new System.Windows.Forms.FlowLayoutPanel();
            this.uxFavs_Library_Title = new System.Windows.Forms.Label();
            this.uxFavs_Library = new System.Windows.Forms.FlowLayoutPanel();
            this.uxFavs_Video_Title = new System.Windows.Forms.Label();
            this.uxFavs_Video = new System.Windows.Forms.FlowLayoutPanel();
            this.uxFavs_Guidelines_Title = new System.Windows.Forms.Label();
            this.uxFavs_Guidelines = new System.Windows.Forms.FlowLayoutPanel();
            this.uxTabs_About = new System.Windows.Forms.TabPage();
            this.uxTabs_Search = new System.Windows.Forms.TabPage();
            this.uxPanel_Search = new System.Windows.Forms.FlowLayoutPanel();
            this.uxSearch_Bar = new System.Windows.Forms.TableLayoutPanel();
            this.uxSearch_Terms = new System.Windows.Forms.TextBox();
            this.uxSearch_Submit = new System.Windows.Forms.Button();
            this.uxSearch_Message = new System.Windows.Forms.Label();
            this.uxSearch_Library_Title = new System.Windows.Forms.Label();
            this.uxSearch_Library = new System.Windows.Forms.FlowLayoutPanel();
            this.uxSearch_Videos_Title = new System.Windows.Forms.Label();
            this.uxSearch_Videos = new System.Windows.Forms.FlowLayoutPanel();
            this.uxSearch_Guidelines_Title = new System.Windows.Forms.Label();
            this.uxSearch_Guidelines = new System.Windows.Forms.FlowLayoutPanel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.printForm1 = new Microsoft.VisualBasic.PowerPacks.Printing.PrintForm(this.components);
            this.uxTitleBar = new RHL.UserControls.Title_Bar();
            this.uxFav = new RHL.UserControls.Set_Fav();
            this.page_About1 = new RHL.UserControls.Page_About();
            this.uxMenu.SuspendLayout();
            this.uxTabs.SuspendLayout();
            this.uxTabs_Library.SuspendLayout();
            this.uxPanel_Library.SuspendLayout();
            this.uxTermHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxTermHeader_Image)).BeginInit();
            this.uxTermHeader_PanelTitle.SuspendLayout();
            this.uxArticleDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxArticleDetail_Image)).BeginInit();
            this.uxArticleDetail_PanelTitle.SuspendLayout();
            this.uxTabs_Guidelines.SuspendLayout();
            this.uxPanel_Guidelines.SuspendLayout();
            this.uxTabs_Videos.SuspendLayout();
            this.uxPanel_Video.SuspendLayout();
            this.uxTabs_Favs.SuspendLayout();
            this.uxPanel_Favs.SuspendLayout();
            this.uxTabs_About.SuspendLayout();
            this.uxTabs_Search.SuspendLayout();
            this.uxPanel_Search.SuspendLayout();
            this.uxSearch_Bar.SuspendLayout();
            this.SuspendLayout();
            // 
            // uxMenu
            // 
            this.uxMenu.BackColor = System.Drawing.SystemColors.Control;
            this.uxMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.uxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uxMenu_Language,
            this.uxMenu_Document});
            resources.ApplyResources(this.uxMenu, "uxMenu");
            this.uxMenu.Name = "uxMenu";
            // 
            // uxMenu_Language
            // 
            this.uxMenu_Language.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uxMenuLanguage_En,
            this.uxMenuLanguage_ZH_HANS,
            this.uxMenuLanguage_FR,
            this.uxMenuLanguage_ES,
            this.uxMenuLanguage_PT_BR});
            this.uxMenu_Language.Name = "uxMenu_Language";
            resources.ApplyResources(this.uxMenu_Language, "uxMenu_Language");
            this.uxMenu_Language.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.uxMenuLanguage_DropDownItemClicked);
            // 
            // uxMenuLanguage_En
            // 
            this.uxMenuLanguage_En.Name = "uxMenuLanguage_En";
            resources.ApplyResources(this.uxMenuLanguage_En, "uxMenuLanguage_En");
            this.uxMenuLanguage_En.Tag = "en";
            // 
            // uxMenuLanguage_ZH_HANS
            // 
            this.uxMenuLanguage_ZH_HANS.Name = "uxMenuLanguage_ZH_HANS";
            resources.ApplyResources(this.uxMenuLanguage_ZH_HANS, "uxMenuLanguage_ZH_HANS");
            this.uxMenuLanguage_ZH_HANS.Tag = "zh-hans";
            // 
            // uxMenuLanguage_FR
            // 
            this.uxMenuLanguage_FR.Name = "uxMenuLanguage_FR";
            resources.ApplyResources(this.uxMenuLanguage_FR, "uxMenuLanguage_FR");
            this.uxMenuLanguage_FR.Tag = "fr";
            // 
            // uxMenuLanguage_ES
            // 
            this.uxMenuLanguage_ES.Name = "uxMenuLanguage_ES";
            resources.ApplyResources(this.uxMenuLanguage_ES, "uxMenuLanguage_ES");
            this.uxMenuLanguage_ES.Tag = "es";
            // 
            // uxMenuLanguage_PT_BR
            // 
            this.uxMenuLanguage_PT_BR.Name = "uxMenuLanguage_PT_BR";
            resources.ApplyResources(this.uxMenuLanguage_PT_BR, "uxMenuLanguage_PT_BR");
            this.uxMenuLanguage_PT_BR.Tag = "pt-br";
            // 
            // uxMenu_Document
            // 
            this.uxMenu_Document.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uxMenuDocument_Print});
            this.uxMenu_Document.Name = "uxMenu_Document";
            resources.ApplyResources(this.uxMenu_Document, "uxMenu_Document");
            // 
            // uxMenuDocument_Print
            // 
            this.uxMenuDocument_Print.Name = "uxMenuDocument_Print";
            resources.ApplyResources(this.uxMenuDocument_Print, "uxMenuDocument_Print");
            this.uxMenuDocument_Print.Click += new System.EventHandler(this.uxMenuDocument_Print_Click);
            // 
            // uxMenuLanguage_RU
            // 
            this.uxMenuLanguage_RU.Name = "uxMenuLanguage_RU";
            resources.ApplyResources(this.uxMenuLanguage_RU, "uxMenuLanguage_RU");
            this.uxMenuLanguage_RU.Tag = "ru";
            // 
            // uxTabs
            // 
            resources.ApplyResources(this.uxTabs, "uxTabs");
            this.uxTabs.Controls.Add(this.uxTabs_Library);
            this.uxTabs.Controls.Add(this.uxTabs_Guidelines);
            this.uxTabs.Controls.Add(this.uxTabs_Videos);
            this.uxTabs.Controls.Add(this.uxTabs_Favs);
            this.uxTabs.Controls.Add(this.uxTabs_About);
            this.uxTabs.Controls.Add(this.uxTabs_Search);
            this.uxTabs.Name = "uxTabs";
            this.uxTabs.SelectedIndex = 0;
            this.uxTabs.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.uxTabs.SelectedIndexChanged += new System.EventHandler(this.uxTabs_SelectedIndexChanged);
            this.uxTabs.Click += new System.EventHandler(this.uxTabs_Click);
            // 
            // uxTabs_Library
            // 
            this.uxTabs_Library.BackColor = System.Drawing.Color.White;
            this.uxTabs_Library.Controls.Add(this.uxPanel_Library);
            resources.ApplyResources(this.uxTabs_Library, "uxTabs_Library");
            this.uxTabs_Library.Name = "uxTabs_Library";
            // 
            // uxPanel_Library
            // 
            resources.ApplyResources(this.uxPanel_Library, "uxPanel_Library");
            this.uxPanel_Library.BackColor = System.Drawing.Color.White;
            this.uxPanel_Library.Controls.Add(this.uxTermHeader);
            this.uxPanel_Library.Controls.Add(this.uxNodeTitle);
            this.uxPanel_Library.Controls.Add(this.uxNodes);
            this.uxPanel_Library.Controls.Add(this.uxTermsTitle);
            this.uxPanel_Library.Controls.Add(this.uxTerms);
            this.uxPanel_Library.Controls.Add(this.uxArticleDetail);
            this.uxPanel_Library.Name = "uxPanel_Library";
            // 
            // uxTermHeader
            // 
            resources.ApplyResources(this.uxTermHeader, "uxTermHeader");
            this.uxTermHeader.BackColor = System.Drawing.Color.Gainsboro;
            this.uxTermHeader.Controls.Add(this.uxTermHeader_Image, 0, 0);
            this.uxTermHeader.Controls.Add(this.uxTermHeader_PanelTitle, 1, 0);
            this.uxTermHeader.Name = "uxTermHeader";
            // 
            // uxTermHeader_Image
            // 
            resources.ApplyResources(this.uxTermHeader_Image, "uxTermHeader_Image");
            this.uxTermHeader_Image.Name = "uxTermHeader_Image";
            this.uxTermHeader_Image.TabStop = false;
            // 
            // uxTermHeader_PanelTitle
            // 
            resources.ApplyResources(this.uxTermHeader_PanelTitle, "uxTermHeader_PanelTitle");
            this.uxTermHeader_PanelTitle.BackColor = System.Drawing.Color.Transparent;
            this.uxTermHeader_PanelTitle.Controls.Add(this.uxTermHeader_Description);
            this.uxTermHeader_PanelTitle.Name = "uxTermHeader_PanelTitle";
            // 
            // uxTermHeader_Description
            // 
            resources.ApplyResources(this.uxTermHeader_Description, "uxTermHeader_Description");
            this.uxTermHeader_Description.Name = "uxTermHeader_Description";
            // 
            // uxNodeTitle
            // 
            resources.ApplyResources(this.uxNodeTitle, "uxNodeTitle");
            this.uxNodeTitle.Name = "uxNodeTitle";
            // 
            // uxNodes
            // 
            resources.ApplyResources(this.uxNodes, "uxNodes");
            this.uxNodes.BackColor = System.Drawing.Color.White;
            this.uxNodes.Name = "uxNodes";
            // 
            // uxTermsTitle
            // 
            resources.ApplyResources(this.uxTermsTitle, "uxTermsTitle");
            this.uxTermsTitle.Name = "uxTermsTitle";
            // 
            // uxTerms
            // 
            resources.ApplyResources(this.uxTerms, "uxTerms");
            this.uxTerms.Name = "uxTerms";
            // 
            // uxArticleDetail
            // 
            resources.ApplyResources(this.uxArticleDetail, "uxArticleDetail");
            this.uxArticleDetail.BackColor = System.Drawing.Color.White;
            this.uxArticleDetail.Controls.Add(this.uxArticleDetail_Image, 0, 0);
            this.uxArticleDetail.Controls.Add(this.uxArticleDetail_PanelTitle, 1, 0);
            this.uxArticleDetail.Controls.Add(this.uxArticleDetail_Body, 0, 1);
            this.uxArticleDetail.Controls.Add(this.uxFav, 2, 0);
            this.uxArticleDetail.Name = "uxArticleDetail";
            // 
            // uxArticleDetail_Image
            // 
            resources.ApplyResources(this.uxArticleDetail_Image, "uxArticleDetail_Image");
            this.uxArticleDetail_Image.Name = "uxArticleDetail_Image";
            this.uxArticleDetail_Image.TabStop = false;
            // 
            // uxArticleDetail_PanelTitle
            // 
            resources.ApplyResources(this.uxArticleDetail_PanelTitle, "uxArticleDetail_PanelTitle");
            this.uxArticleDetail_PanelTitle.BackColor = System.Drawing.Color.White;
            this.uxArticleDetail_PanelTitle.Controls.Add(this.uxArticleDetail_Title);
            this.uxArticleDetail_PanelTitle.Controls.Add(this.uxArticleDetail_Date);
            this.uxArticleDetail_PanelTitle.Name = "uxArticleDetail_PanelTitle";
            // 
            // uxArticleDetail_Title
            // 
            resources.ApplyResources(this.uxArticleDetail_Title, "uxArticleDetail_Title");
            this.uxArticleDetail_Title.Name = "uxArticleDetail_Title";
            // 
            // uxArticleDetail_Date
            // 
            resources.ApplyResources(this.uxArticleDetail_Date, "uxArticleDetail_Date");
            this.uxArticleDetail_Date.Name = "uxArticleDetail_Date";
            // 
            // uxArticleDetail_Body
            // 
            this.uxArticleDetail.SetColumnSpan(this.uxArticleDetail_Body, 3);
            resources.ApplyResources(this.uxArticleDetail_Body, "uxArticleDetail_Body");
            this.uxArticleDetail_Body.Name = "uxArticleDetail_Body";
            this.uxArticleDetail_Body.ScriptErrorsSuppressed = true;
            this.uxArticleDetail_Body.ScrollBarsEnabled = false;
            // 
            // uxTabs_Guidelines
            // 
            this.uxTabs_Guidelines.Controls.Add(this.uxPanel_Guidelines);
            resources.ApplyResources(this.uxTabs_Guidelines, "uxTabs_Guidelines");
            this.uxTabs_Guidelines.Name = "uxTabs_Guidelines";
            this.uxTabs_Guidelines.UseVisualStyleBackColor = true;
            // 
            // uxPanel_Guidelines
            // 
            resources.ApplyResources(this.uxPanel_Guidelines, "uxPanel_Guidelines");
            this.uxPanel_Guidelines.BackColor = System.Drawing.Color.White;
            this.uxPanel_Guidelines.Controls.Add(this.uxGuidelinesTitle);
            this.uxPanel_Guidelines.Controls.Add(this.uxGuidelines);
            this.uxPanel_Guidelines.Name = "uxPanel_Guidelines";
            // 
            // uxGuidelinesTitle
            // 
            resources.ApplyResources(this.uxGuidelinesTitle, "uxGuidelinesTitle");
            this.uxGuidelinesTitle.Name = "uxGuidelinesTitle";
            // 
            // uxGuidelines
            // 
            resources.ApplyResources(this.uxGuidelines, "uxGuidelines");
            this.uxGuidelines.BackColor = System.Drawing.Color.White;
            this.uxGuidelines.Name = "uxGuidelines";
            // 
            // uxTabs_Videos
            // 
            this.uxTabs_Videos.Controls.Add(this.uxPanel_Video);
            resources.ApplyResources(this.uxTabs_Videos, "uxTabs_Videos");
            this.uxTabs_Videos.Name = "uxTabs_Videos";
            this.uxTabs_Videos.UseVisualStyleBackColor = true;
            // 
            // uxPanel_Video
            // 
            resources.ApplyResources(this.uxPanel_Video, "uxPanel_Video");
            this.uxPanel_Video.BackColor = System.Drawing.Color.White;
            this.uxPanel_Video.Controls.Add(this.uxVideosTitle);
            this.uxPanel_Video.Controls.Add(this.uxVideos);
            this.uxPanel_Video.Name = "uxPanel_Video";
            // 
            // uxVideosTitle
            // 
            resources.ApplyResources(this.uxVideosTitle, "uxVideosTitle");
            this.uxVideosTitle.Name = "uxVideosTitle";
            // 
            // uxVideos
            // 
            resources.ApplyResources(this.uxVideos, "uxVideos");
            this.uxVideos.BackColor = System.Drawing.Color.White;
            this.uxVideos.Name = "uxVideos";
            // 
            // uxTabs_Favs
            // 
            this.uxTabs_Favs.Controls.Add(this.uxPanel_Favs);
            resources.ApplyResources(this.uxTabs_Favs, "uxTabs_Favs");
            this.uxTabs_Favs.Name = "uxTabs_Favs";
            this.uxTabs_Favs.Tag = "favs";
            this.uxTabs_Favs.UseVisualStyleBackColor = true;
            // 
            // uxPanel_Favs
            // 
            this.uxPanel_Favs.Controls.Add(this.uxFavs_Library_Title);
            this.uxPanel_Favs.Controls.Add(this.uxFavs_Library);
            this.uxPanel_Favs.Controls.Add(this.uxFavs_Video_Title);
            this.uxPanel_Favs.Controls.Add(this.uxFavs_Video);
            this.uxPanel_Favs.Controls.Add(this.uxFavs_Guidelines_Title);
            this.uxPanel_Favs.Controls.Add(this.uxFavs_Guidelines);
            resources.ApplyResources(this.uxPanel_Favs, "uxPanel_Favs");
            this.uxPanel_Favs.Name = "uxPanel_Favs";
            // 
            // uxFavs_Library_Title
            // 
            resources.ApplyResources(this.uxFavs_Library_Title, "uxFavs_Library_Title");
            this.uxFavs_Library_Title.Name = "uxFavs_Library_Title";
            // 
            // uxFavs_Library
            // 
            resources.ApplyResources(this.uxFavs_Library, "uxFavs_Library");
            this.uxFavs_Library.Name = "uxFavs_Library";
            // 
            // uxFavs_Video_Title
            // 
            resources.ApplyResources(this.uxFavs_Video_Title, "uxFavs_Video_Title");
            this.uxFavs_Video_Title.Name = "uxFavs_Video_Title";
            // 
            // uxFavs_Video
            // 
            resources.ApplyResources(this.uxFavs_Video, "uxFavs_Video");
            this.uxFavs_Video.Name = "uxFavs_Video";
            // 
            // uxFavs_Guidelines_Title
            // 
            resources.ApplyResources(this.uxFavs_Guidelines_Title, "uxFavs_Guidelines_Title");
            this.uxFavs_Guidelines_Title.Name = "uxFavs_Guidelines_Title";
            // 
            // uxFavs_Guidelines
            // 
            resources.ApplyResources(this.uxFavs_Guidelines, "uxFavs_Guidelines");
            this.uxFavs_Guidelines.Name = "uxFavs_Guidelines";
            // 
            // uxTabs_About
            // 
            resources.ApplyResources(this.uxTabs_About, "uxTabs_About");
            this.uxTabs_About.Controls.Add(this.page_About1);
            this.uxTabs_About.Name = "uxTabs_About";
            this.uxTabs_About.UseVisualStyleBackColor = true;
            // 
            // uxTabs_Search
            // 
            this.uxTabs_Search.Controls.Add(this.uxPanel_Search);
            resources.ApplyResources(this.uxTabs_Search, "uxTabs_Search");
            this.uxTabs_Search.Name = "uxTabs_Search";
            this.uxTabs_Search.UseVisualStyleBackColor = true;
            // 
            // uxPanel_Search
            // 
            resources.ApplyResources(this.uxPanel_Search, "uxPanel_Search");
            this.uxPanel_Search.Controls.Add(this.uxSearch_Bar);
            this.uxPanel_Search.Controls.Add(this.uxSearch_Message);
            this.uxPanel_Search.Controls.Add(this.uxSearch_Library_Title);
            this.uxPanel_Search.Controls.Add(this.uxSearch_Library);
            this.uxPanel_Search.Controls.Add(this.uxSearch_Videos_Title);
            this.uxPanel_Search.Controls.Add(this.uxSearch_Videos);
            this.uxPanel_Search.Controls.Add(this.uxSearch_Guidelines_Title);
            this.uxPanel_Search.Controls.Add(this.uxSearch_Guidelines);
            this.uxPanel_Search.Name = "uxPanel_Search";
            // 
            // uxSearch_Bar
            // 
            resources.ApplyResources(this.uxSearch_Bar, "uxSearch_Bar");
            this.uxSearch_Bar.Controls.Add(this.uxSearch_Terms, 1, 0);
            this.uxSearch_Bar.Controls.Add(this.uxSearch_Submit, 2, 0);
            this.uxSearch_Bar.Name = "uxSearch_Bar";
            // 
            // uxSearch_Terms
            // 
            resources.ApplyResources(this.uxSearch_Terms, "uxSearch_Terms");
            this.uxSearch_Terms.Name = "uxSearch_Terms";
            // 
            // uxSearch_Submit
            // 
            this.uxSearch_Submit.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.uxSearch_Submit, "uxSearch_Submit");
            this.uxSearch_Submit.Name = "uxSearch_Submit";
            this.uxSearch_Submit.UseVisualStyleBackColor = true;
            this.uxSearch_Submit.Click += new System.EventHandler(this.uxSearch_Submit_Click);
            // 
            // uxSearch_Message
            // 
            resources.ApplyResources(this.uxSearch_Message, "uxSearch_Message");
            this.uxSearch_Message.Name = "uxSearch_Message";
            // 
            // uxSearch_Library_Title
            // 
            resources.ApplyResources(this.uxSearch_Library_Title, "uxSearch_Library_Title");
            this.uxSearch_Library_Title.Name = "uxSearch_Library_Title";
            // 
            // uxSearch_Library
            // 
            resources.ApplyResources(this.uxSearch_Library, "uxSearch_Library");
            this.uxSearch_Library.Name = "uxSearch_Library";
            // 
            // uxSearch_Videos_Title
            // 
            resources.ApplyResources(this.uxSearch_Videos_Title, "uxSearch_Videos_Title");
            this.uxSearch_Videos_Title.Name = "uxSearch_Videos_Title";
            // 
            // uxSearch_Videos
            // 
            resources.ApplyResources(this.uxSearch_Videos, "uxSearch_Videos");
            this.uxSearch_Videos.Name = "uxSearch_Videos";
            // 
            // uxSearch_Guidelines_Title
            // 
            resources.ApplyResources(this.uxSearch_Guidelines_Title, "uxSearch_Guidelines_Title");
            this.uxSearch_Guidelines_Title.Name = "uxSearch_Guidelines_Title";
            // 
            // uxSearch_Guidelines
            // 
            resources.ApplyResources(this.uxSearch_Guidelines, "uxSearch_Guidelines");
            this.uxSearch_Guidelines.Name = "uxSearch_Guidelines";
            // 
            // printForm1
            // 
            this.printForm1.DocumentName = "document";
            this.printForm1.Form = this;
            this.printForm1.PrintAction = System.Drawing.Printing.PrintAction.PrintToPrinter;
            this.printForm1.PrinterSettings = ((System.Drawing.Printing.PrinterSettings)(resources.GetObject("printForm1.PrinterSettings")));
            this.printForm1.PrintFileName = null;
            // 
            // uxTitleBar
            // 
            this.uxTitleBar.ActionVisible = false;
            resources.ApplyResources(this.uxTitleBar, "uxTitleBar");
            this.uxTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(109)))), ((int)(((byte)(174)))));
            this.uxTitleBar.Name = "uxTitleBar";
            this.uxTitleBar.Title = "Title Text";
            // 
            // uxFav
            // 
            resources.ApplyResources(this.uxFav, "uxFav");
            this.uxFav.Name = "uxFav";
            this.uxFav.Nid = 0;
            this.uxFav.Type = "";
            // 
            // page_About1
            // 
            resources.ApplyResources(this.page_About1, "page_About1");
            this.page_About1.BackColor = System.Drawing.SystemColors.Window;
            this.page_About1.Name = "page_About1";
            // 
            // RHLApp2
            // 
            this.AcceptButton = this.uxSearch_Submit;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.uxTitleBar);
            this.Controls.Add(this.uxTabs);
            this.Controls.Add(this.uxMenu);
            this.KeyPreview = true;
            this.MainMenuStrip = this.uxMenu;
            this.Name = "RHLApp2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResizeBegin += new System.EventHandler(this.RHLApp2_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.RHLApp2_ResizeEnd);
            this.uxMenu.ResumeLayout(false);
            this.uxMenu.PerformLayout();
            this.uxTabs.ResumeLayout(false);
            this.uxTabs_Library.ResumeLayout(false);
            this.uxTabs_Library.PerformLayout();
            this.uxPanel_Library.ResumeLayout(false);
            this.uxPanel_Library.PerformLayout();
            this.uxTermHeader.ResumeLayout(false);
            this.uxTermHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxTermHeader_Image)).EndInit();
            this.uxTermHeader_PanelTitle.ResumeLayout(false);
            this.uxTermHeader_PanelTitle.PerformLayout();
            this.uxArticleDetail.ResumeLayout(false);
            this.uxArticleDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxArticleDetail_Image)).EndInit();
            this.uxArticleDetail_PanelTitle.ResumeLayout(false);
            this.uxArticleDetail_PanelTitle.PerformLayout();
            this.uxTabs_Guidelines.ResumeLayout(false);
            this.uxTabs_Guidelines.PerformLayout();
            this.uxPanel_Guidelines.ResumeLayout(false);
            this.uxPanel_Guidelines.PerformLayout();
            this.uxTabs_Videos.ResumeLayout(false);
            this.uxTabs_Videos.PerformLayout();
            this.uxPanel_Video.ResumeLayout(false);
            this.uxPanel_Video.PerformLayout();
            this.uxTabs_Favs.ResumeLayout(false);
            this.uxPanel_Favs.ResumeLayout(false);
            this.uxPanel_Favs.PerformLayout();
            this.uxTabs_About.ResumeLayout(false);
            this.uxTabs_Search.ResumeLayout(false);
            this.uxTabs_Search.PerformLayout();
            this.uxPanel_Search.ResumeLayout(false);
            this.uxPanel_Search.PerformLayout();
            this.uxSearch_Bar.ResumeLayout(false);
            this.uxSearch_Bar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip uxMenu;
        private System.Windows.Forms.ToolStripMenuItem uxMenu_Language;
        private System.Windows.Forms.ToolStripMenuItem uxMenuLanguage_En;
        private System.Windows.Forms.ToolStripMenuItem uxMenuLanguage_ZH_HANS;
        private System.Windows.Forms.ToolStripMenuItem uxMenuLanguage_FR;
        private System.Windows.Forms.ToolStripMenuItem uxMenuLanguage_RU;
        private System.Windows.Forms.ToolStripMenuItem uxMenuLanguage_ES;
        private System.Windows.Forms.ToolStripMenuItem uxMenuLanguage_PT_BR;
        private System.Windows.Forms.TabControl uxTabs;
        private System.Windows.Forms.TabPage uxTabs_Library;
        private System.Windows.Forms.TabPage uxTabs_Guidelines;
        private System.Windows.Forms.TabPage uxTabs_Videos;
        private System.Windows.Forms.TabPage uxTabs_Favs;
        private System.Windows.Forms.FlowLayoutPanel uxPanel_Library;
        private System.Windows.Forms.Label uxNodeTitle;
        private System.Windows.Forms.FlowLayoutPanel uxNodes;
        private System.Windows.Forms.Label uxTermsTitle;
        private System.Windows.Forms.TableLayoutPanel uxTerms;
        private System.Windows.Forms.TableLayoutPanel uxArticleDetail;
        private System.Windows.Forms.PictureBox uxArticleDetail_Image;
        private System.Windows.Forms.Label uxArticleDetail_Title;
        private System.Windows.Forms.FlowLayoutPanel uxArticleDetail_PanelTitle;
        private System.Windows.Forms.Label uxArticleDetail_Date;
        private System.Windows.Forms.WebBrowser uxArticleDetail_Body;
        private System.Windows.Forms.TableLayoutPanel uxTermHeader;
        private System.Windows.Forms.PictureBox uxTermHeader_Image;
        private System.Windows.Forms.FlowLayoutPanel uxTermHeader_PanelTitle;
        private System.Windows.Forms.Label uxTermHeader_Description;

        private System.Windows.Forms.FlowLayoutPanel uxPanel_Video;
        private System.Windows.Forms.Label uxVideosTitle;
        private System.Windows.Forms.FlowLayoutPanel uxVideos;

        private System.Windows.Forms.FlowLayoutPanel uxPanel_Guidelines;
        private System.Windows.Forms.Label uxGuidelinesTitle;
        private System.Windows.Forms.FlowLayoutPanel uxGuidelines;

        private System.Windows.Forms.FlowLayoutPanel uxPanel_Favs;
        private System.Windows.Forms.Label uxFavs_Library_Title;
        private System.Windows.Forms.FlowLayoutPanel uxFavs_Library;
        private System.Windows.Forms.Label uxFavs_Video_Title;
        private System.Windows.Forms.FlowLayoutPanel uxFavs_Video;
        private System.Windows.Forms.Label uxFavs_Guidelines_Title;
        private System.Windows.Forms.FlowLayoutPanel uxFavs_Guidelines;
        private UserControls.Set_Fav uxFav;
        private UserControls.Title_Bar uxTitleBar;
        private System.Windows.Forms.TabPage uxTabs_About;
        private System.Windows.Forms.TabPage uxTabs_Search;
        private System.Windows.Forms.FlowLayoutPanel uxPanel_Search;
        private System.Windows.Forms.TableLayoutPanel uxSearch_Bar;
        private System.Windows.Forms.TextBox uxSearch_Terms;
        private System.Windows.Forms.Button uxSearch_Submit;
        private System.Windows.Forms.Label uxSearch_Library_Title;
        private System.Windows.Forms.FlowLayoutPanel uxSearch_Library;
        private System.Windows.Forms.Label uxSearch_Videos_Title;
        private System.Windows.Forms.FlowLayoutPanel uxSearch_Videos;
        private System.Windows.Forms.Label uxSearch_Guidelines_Title;
        private System.Windows.Forms.FlowLayoutPanel uxSearch_Guidelines;
        private System.Windows.Forms.Label uxSearch_Message;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private UserControls.Page_About page_About1;
        private System.Windows.Forms.ToolStripMenuItem uxMenu_Document;
        private System.Windows.Forms.ToolStripMenuItem uxMenuDocument_Print;
        private Microsoft.VisualBasic.PowerPacks.Printing.PrintForm printForm1;
    }
}