﻿namespace RHL
{
    partial class Player
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Player));
            this.uxBrowser = new System.Windows.Forms.WebBrowser();
            this.uxDescription = new System.Windows.Forms.Label();
            this.uxFav = new RHL.UserControls.Set_Fav();
            this.SuspendLayout();
            // 
            // uxBrowser
            // 
            this.uxBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxBrowser.Location = new System.Drawing.Point(-10, 12);
            this.uxBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.uxBrowser.Name = "uxBrowser";
            this.uxBrowser.ScrollBarsEnabled = false;
            this.uxBrowser.Size = new System.Drawing.Size(1410, 772);
            this.uxBrowser.TabIndex = 0;
            // 
            // uxDescription
            // 
            this.uxDescription.AutoEllipsis = true;
            this.uxDescription.BackColor = System.Drawing.Color.Transparent;
            this.uxDescription.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxDescription.Location = new System.Drawing.Point(12, 787);
            this.uxDescription.Name = "uxDescription";
            this.uxDescription.Size = new System.Drawing.Size(1183, 113);
            this.uxDescription.TabIndex = 1;
            this.uxDescription.Text = "description";
            this.uxDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uxFav
            // 
            this.uxFav.Location = new System.Drawing.Point(1229, 787);
            this.uxFav.Name = "uxFav";
            this.uxFav.Nid = 0;
            this.uxFav.Size = new System.Drawing.Size(130, 113);
            this.uxFav.TabIndex = 2;
            this.uxFav.Type = "";
            // 
            // Player
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1400, 914);
            this.Controls.Add(this.uxFav);
            this.Controls.Add(this.uxDescription);
            this.Controls.Add(this.uxBrowser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Player";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Player";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser uxBrowser;
        private System.Windows.Forms.Label uxDescription;
        private UserControls.Set_Fav uxFav;
    }
}