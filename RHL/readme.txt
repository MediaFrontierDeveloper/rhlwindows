﻿------------------------------------------------------------------
RHL Windows App
------------------------------------------------------------------

v0.9.4.0
##################################################################
#
#
### UI
 - New splash screen
 - New app icon
 - New form icon(s)
 - New "offline" message in sync spalsh
 - Offline label in form title

### Backend
 - Added Rhl.lib.Session namespace
 - Added session manager to persist flags in app
 - Added sticky propery to INode and Node schemas
 - Added sticky property to video DB schema and queries / manager
 - Added ordering to video queries
 - Data pre-synced (test server/MF) to 2016-10-27 07:00 UTC
 - Updated query to retrieve "new" of video and guideline types

v0.9.3.0
##################################################################
#
#
### UI
 - Brand new flow layout for grids
 - Strips erroneous html tags from term descriptions
 - User language preference now gets saved
 - Caching article thumbnails for better performance
 - "New in RHL" on the main from now exactly reflects to front page of the website
 - /lang/new added to web API to serve front page view
 - Video section added
 - Video player added
 - Ability to favourite a video
 - Favourite count in navigation tab bar

### Backend
 - Updated Russian article sync
 - Added new RHL.lib.Sync namespace
 - Renamed "SyncData" class to "SyncManager" for API consistency
 - Added a cache manager to RHL.lib
 - Added namespace RHL.lib.Caching
 - Added "type" to articles table (defaults to 'rhl_content_article')
 - Added CONTENT_TYPE_ARTICLE key to string resources
 - New generic menu selection handler
 - Node interface added, with concrete class references removed from user controls
 - "Article_Teaser" amd "Article_Grid" renamed to "Node_Teaser" and "Node_Grid" respectively
 - "New [in RHL]" methods added to SyncManager and DB manager
 - Video type created with DB table "videos" in backend
 - Video URL format changed on web API to use non-legacy YouTube style
 - Integration into favourites for video node type
