﻿namespace RHL.UserControls
{
    partial class Node_Teaser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxImage = new System.Windows.Forms.PictureBox();
            this.uxPanelLabel = new System.Windows.Forms.FlowLayoutPanel();
            this.uxTitle = new System.Windows.Forms.Label();
            this.uxDate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uxImage)).BeginInit();
            this.uxPanelLabel.SuspendLayout();
            this.SuspendLayout();
            // 
            // uxImage
            // 
            this.uxImage.BackColor = System.Drawing.Color.Transparent;
            this.uxImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.uxImage.Location = new System.Drawing.Point(0, 0);
            this.uxImage.Margin = new System.Windows.Forms.Padding(0);
            this.uxImage.Name = "uxImage";
            this.uxImage.Size = new System.Drawing.Size(168, 206);
            this.uxImage.TabIndex = 0;
            this.uxImage.TabStop = false;
            this.uxImage.Click += new System.EventHandler(this.Node_Teaser_Click);
            // 
            // uxPanelLabel
            // 
            this.uxPanelLabel.AutoScroll = true;
            this.uxPanelLabel.AutoSize = true;
            this.uxPanelLabel.BackColor = System.Drawing.Color.Black;
            this.uxPanelLabel.Controls.Add(this.uxTitle);
            this.uxPanelLabel.Controls.Add(this.uxDate);
            this.uxPanelLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uxPanelLabel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.uxPanelLabel.Location = new System.Drawing.Point(0, 141);
            this.uxPanelLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.uxPanelLabel.Name = "uxPanelLabel";
            this.uxPanelLabel.Padding = new System.Windows.Forms.Padding(5);
            this.uxPanelLabel.Size = new System.Drawing.Size(168, 59);
            this.uxPanelLabel.TabIndex = 1;
            this.uxPanelLabel.WrapContents = false;
            this.uxPanelLabel.Click += new System.EventHandler(this.Node_Teaser_Click);
            // 
            // uxTitle
            // 
            this.uxTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxTitle.AutoSize = true;
            this.uxTitle.Font = new System.Drawing.Font("Arial", 10F);
            this.uxTitle.ForeColor = System.Drawing.Color.White;
            this.uxTitle.Location = new System.Drawing.Point(8, 5);
            this.uxTitle.Name = "uxTitle";
            this.uxTitle.Size = new System.Drawing.Size(66, 23);
            this.uxTitle.TabIndex = 0;
            this.uxTitle.Text = "uxTitle";
            this.uxTitle.Click += new System.EventHandler(this.Node_Teaser_Click);
            // 
            // uxDate
            // 
            this.uxDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxDate.AutoSize = true;
            this.uxPanelLabel.SetFlowBreak(this.uxDate, true);
            this.uxDate.Font = new System.Drawing.Font("Arial", 9F);
            this.uxDate.ForeColor = System.Drawing.Color.White;
            this.uxDate.Location = new System.Drawing.Point(8, 33);
            this.uxDate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.uxDate.Name = "uxDate";
            this.uxDate.Size = new System.Drawing.Size(66, 21);
            this.uxDate.TabIndex = 1;
            this.uxDate.Text = "uxDate";
            this.uxDate.Click += new System.EventHandler(this.Node_Teaser_Click);
            // 
            // Node_Teaser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.uxPanelLabel);
            this.Controls.Add(this.uxImage);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Margin = new System.Windows.Forms.Padding(0, 5, 10, 5);
            this.Name = "Node_Teaser";
            this.Size = new System.Drawing.Size(168, 200);
            this.Load += new System.EventHandler(this.Node_Teaser_Load);
            this.Click += new System.EventHandler(this.Node_Teaser_Click);
            ((System.ComponentModel.ISupportInitialize)(this.uxImage)).EndInit();
            this.uxPanelLabel.ResumeLayout(false);
            this.uxPanelLabel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox uxImage;
        private System.Windows.Forms.FlowLayoutPanel uxPanelLabel;
        private System.Windows.Forms.Label uxTitle;
        private System.Windows.Forms.Label uxDate;
    }
}
