﻿namespace RHL.UserControls
{
    partial class Page_About
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Page_About));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.uxAbout_Title1 = new System.Windows.Forms.Label();
            this.uxAbout_Text1 = new System.Windows.Forms.Label();
            this.uxAbout_Title2 = new System.Windows.Forms.Label();
            this.uxAbout_Text2 = new System.Windows.Forms.Label();
            this.uxAbout_Title3 = new System.Windows.Forms.Label();
            this.uxAbout_Text3 = new System.Windows.Forms.Label();
            this.uxLogo = new System.Windows.Forms.PictureBox();
            this.uxLogoTitle = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.15105F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.84895F));
            this.tableLayoutPanel1.Controls.Add(this.uxAbout_Title1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.uxAbout_Text1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.uxAbout_Title2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.uxAbout_Text2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.uxAbout_Title3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.uxAbout_Text3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.uxLogo, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.uxLogoTitle, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 50, 0, 0);
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(556, 808);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // uxAbout_Title1
            // 
            this.uxAbout_Title1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.uxAbout_Title1, 2);
            this.uxAbout_Title1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxAbout_Title1.Location = new System.Drawing.Point(3, 312);
            this.uxAbout_Title1.Name = "uxAbout_Title1";
            this.uxAbout_Title1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.uxAbout_Title1.Size = new System.Drawing.Size(170, 37);
            this.uxAbout_Title1.TabIndex = 0;
            this.uxAbout_Title1.Text = "uxAbout_Title1";
            // 
            // uxAbout_Text1
            // 
            this.uxAbout_Text1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.uxAbout_Text1, 2);
            this.uxAbout_Text1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxAbout_Text1.Location = new System.Drawing.Point(3, 349);
            this.uxAbout_Text1.Name = "uxAbout_Text1";
            this.uxAbout_Text1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.uxAbout_Text1.Size = new System.Drawing.Size(109, 28);
            this.uxAbout_Text1.TabIndex = 1;
            this.uxAbout_Text1.Text = "uxAbout_Text1";
            // 
            // uxAbout_Title2
            // 
            this.uxAbout_Title2.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.uxAbout_Title2, 2);
            this.uxAbout_Title2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxAbout_Title2.Location = new System.Drawing.Point(3, 377);
            this.uxAbout_Title2.Name = "uxAbout_Title2";
            this.uxAbout_Title2.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.uxAbout_Title2.Size = new System.Drawing.Size(170, 37);
            this.uxAbout_Title2.TabIndex = 2;
            this.uxAbout_Title2.Text = "uxAbout_Title2";
            // 
            // uxAbout_Text2
            // 
            this.uxAbout_Text2.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.uxAbout_Text2, 2);
            this.uxAbout_Text2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxAbout_Text2.Location = new System.Drawing.Point(3, 414);
            this.uxAbout_Text2.Name = "uxAbout_Text2";
            this.uxAbout_Text2.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.uxAbout_Text2.Size = new System.Drawing.Size(109, 28);
            this.uxAbout_Text2.TabIndex = 3;
            this.uxAbout_Text2.Text = "uxAbout_Text2";
            // 
            // uxAbout_Title3
            // 
            this.uxAbout_Title3.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.uxAbout_Title3, 2);
            this.uxAbout_Title3.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxAbout_Title3.Location = new System.Drawing.Point(3, 442);
            this.uxAbout_Title3.Name = "uxAbout_Title3";
            this.uxAbout_Title3.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.uxAbout_Title3.Size = new System.Drawing.Size(170, 37);
            this.uxAbout_Title3.TabIndex = 4;
            this.uxAbout_Title3.Text = "uxAbout_Title3";
            // 
            // uxAbout_Text3
            // 
            this.uxAbout_Text3.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.uxAbout_Text3, 2);
            this.uxAbout_Text3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxAbout_Text3.Location = new System.Drawing.Point(3, 479);
            this.uxAbout_Text3.Name = "uxAbout_Text3";
            this.uxAbout_Text3.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.uxAbout_Text3.Size = new System.Drawing.Size(109, 28);
            this.uxAbout_Text3.TabIndex = 5;
            this.uxAbout_Text3.Text = "uxAbout_Text3";
            // 
            // uxLogo
            // 
            this.uxLogo.Image = ((System.Drawing.Image)(resources.GetObject("uxLogo.Image")));
            this.uxLogo.Location = new System.Drawing.Point(3, 53);
            this.uxLogo.Name = "uxLogo";
            this.uxLogo.Size = new System.Drawing.Size(144, 256);
            this.uxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.uxLogo.TabIndex = 6;
            this.uxLogo.TabStop = false;
            // 
            // uxLogoTitle
            // 
            this.uxLogoTitle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.uxLogoTitle.AutoSize = true;
            this.uxLogoTitle.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxLogoTitle.Location = new System.Drawing.Point(153, 160);
            this.uxLogoTitle.Name = "uxLogoTitle";
            this.uxLogoTitle.Size = new System.Drawing.Size(206, 42);
            this.uxLogoTitle.TabIndex = 7;
            this.uxLogoTitle.Text = "uxLogoTitle";
            // 
            // Page_About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Page_About";
            this.Size = new System.Drawing.Size(573, 826);
            this.Load += new System.EventHandler(this.Page_About_Load);
            this.Enter += new System.EventHandler(this.Page_About_Enter);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label uxAbout_Title1;
        private System.Windows.Forms.Label uxAbout_Text1;
        private System.Windows.Forms.Label uxAbout_Title2;
        private System.Windows.Forms.Label uxAbout_Text2;
        private System.Windows.Forms.Label uxAbout_Title3;
        private System.Windows.Forms.Label uxAbout_Text3;
        private System.Windows.Forms.PictureBox uxLogo;
        private System.Windows.Forms.Label uxLogoTitle;
    }
}
