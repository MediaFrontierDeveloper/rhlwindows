﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RHL.lib.Schema;
using RHL.lib.Text;

namespace RHL.UserControls
{
    public partial class Term_Header : UserControl
    {
        public Term Term { get; set; }
        public Term_Header()
        {
            InitializeComponent();
        }
        public void LoadHeader()
        {
            if(Term != null)
            {
                uxDescription.Text = Parser.ScrubHtml(Term.Description);
                uxImage.Image = Term.ThumbNail;
                this.Visible = true;
            }
            else
            {
                this.Visible = false;
            }
        }
    }
}
