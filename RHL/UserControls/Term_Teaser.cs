﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RHL.lib.Schema;
using RHL.lib.Media;

namespace RHL.UserControls
{
    public partial class Term_Teaser : UserControl
    {
        public Term term { get; private set; }

        public Term_Teaser(Term term)
        {
            InitializeComponent();
            this.term = term;
            if(term.ParentTid == 0)
            {
                uxImage.Visible = true;
            }
        }

        private void Term_Teaser_Load(object sender, EventArgs e)
        {
            this.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            //uxTid.Text = this.term.Tid.ToString();
            uxName.Text = this.term.Name;
            uxImage.Image = ImageManager.FixedSize(this.term.ThumbNail, 300, 150, false);
        }

        private event EventHandler<Term> _termClick;
        public event EventHandler<Term> TermClick
        {
            add
            {
                _termClick -= value;
                _termClick += value;
            }
            remove
            {
                _termClick -= value;
            }
        }

        private void Term_Teaser_Click(object sender, EventArgs e)
        {
            if (this._termClick != null)
                this._termClick(this, term);
        }



    }
}
