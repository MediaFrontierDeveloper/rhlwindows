﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Globalization;
using System.Reflection;

namespace RHL.UserControls
{
    public partial class Page_About : UserControl
    {
        ResourceManager rm;

        public Page_About()
        {
            InitializeComponent();
            rm = new ResourceManager("RHL.Resources.strings", Assembly.Load("RHL"));
        }

        private void Page_About_Load(object sender, EventArgs e)
        {

        }

        public void SetText(CultureInfo culture)
        {
            uxLogoTitle.Text = rm.GetString("about_header_title", culture);
            uxAbout_Title1.Text = rm.GetString("about_title_1", culture);
            uxAbout_Text1.Text = rm.GetString("about_text_1", culture);

            uxAbout_Title2.Text = rm.GetString("about_title_2", culture);
            uxAbout_Text2.Text = rm.GetString("about_text_2", culture);

            uxAbout_Title3.Text = rm.GetString("about_title_3", culture);
            uxAbout_Text3.Text = rm.GetString("about_text_3", culture);
        }

        private void Page_About_Enter(object sender, EventArgs e)
        {

        }
    }
}
