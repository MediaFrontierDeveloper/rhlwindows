﻿using System;
using System.Drawing;
using System.Windows.Forms;
using RHL.lib.Schema;
using RHL.lib.Media;
using RHL.lib.Cache;

namespace RHL.UserControls
{
    public partial class Node_Teaser : UserControl
    {
        public INode node { get; set; }
    
        public Node_Teaser(INode node)
        {
            InitializeComponent();
            this.node = node;
            LoadNodeTeaser();
        }
        
        public void LoadNodeTeaser()
        {
            uxTitle.Text = this.node.Title;
            if (this.node.IsFav == true)
            {
                uxTitle.Text = string.Format("{0} {1}", uxTitle.Text, ((char)0x2605).ToString());
            }
            uxDate.Text = this.node.DisplayDate;
            var i = this.node.ImageData;

            // lets see if the latest version of this thumbnail is in the cache
            Image thumb = null;
            int width = 225;
            int height = 206;
            string cacheKey = string.Format("{0}_{1}_{2}_{3}", node.CacheKey, width.ToString(), height.ToString(), node.LangCode);
            thumb = CacheManager.Get<Image>(cacheKey);
            if (thumb == null)
            {
                if (this.node.ThumbNail != null)
                {
                    thumb = ImageManager.FixedSize(this.node.ThumbNail, width, height, true);
                    CacheManager.Add(thumb, cacheKey);
                }
                else
                {
                    thumb = Assets.GetImage("default_thumb.png");
                }
            }
            uxImage.Image = thumb;
        }

        private event EventHandler<INode> _nodeClick;
        public event EventHandler<INode> NodeClick
        {
            add
            {
                _nodeClick -= value;
                _nodeClick += value;
            }
            remove
            {
                _nodeClick -= value;
            }
        }

        private void Node_Teaser_Click(object sender, EventArgs e)
        {
            if (this._nodeClick != null)
                this._nodeClick(this, node);
        }

        private void Node_Teaser_Load(object sender, EventArgs e)
        {

        }
    }
}
