﻿namespace RHL.UserControls
{
    partial class Title_Bar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Title_Bar));
            this.uxPanelTitle = new System.Windows.Forms.TableLayoutPanel();
            this.uxTitle = new System.Windows.Forms.Label();
            this.uxTitleImage = new System.Windows.Forms.PictureBox();
            this.uxTiltleBar_Action = new System.Windows.Forms.Button();
            this.uxTitlebar_Action_Tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.uxLogoWho = new System.Windows.Forms.PictureBox();
            this.uxPanelTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxTitleImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uxLogoWho)).BeginInit();
            this.SuspendLayout();
            // 
            // uxPanelTitle
            // 
            this.uxPanelTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxPanelTitle.AutoSize = true;
            this.uxPanelTitle.ColumnCount = 5;
            this.uxPanelTitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxPanelTitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxPanelTitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.uxPanelTitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.uxPanelTitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 747F));
            this.uxPanelTitle.Controls.Add(this.uxTitle, 2, 0);
            this.uxPanelTitle.Controls.Add(this.uxTitleImage, 1, 0);
            this.uxPanelTitle.Controls.Add(this.uxTiltleBar_Action, 0, 0);
            this.uxPanelTitle.Controls.Add(this.uxLogoWho, 4, 0);
            this.uxPanelTitle.Location = new System.Drawing.Point(3, 2);
            this.uxPanelTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.uxPanelTitle.Name = "uxPanelTitle";
            this.uxPanelTitle.RowCount = 1;
            this.uxPanelTitle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.uxPanelTitle.Size = new System.Drawing.Size(2104, 92);
            this.uxPanelTitle.TabIndex = 0;
            // 
            // uxTitle
            // 
            this.uxTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxTitle.AutoSize = true;
            this.uxTitle.BackColor = System.Drawing.Color.Transparent;
            this.uxTitle.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxTitle.ForeColor = System.Drawing.Color.White;
            this.uxTitle.Location = new System.Drawing.Point(440, 0);
            this.uxTitle.Margin = new System.Windows.Forms.Padding(16, 0, 3, 0);
            this.uxTitle.Name = "uxTitle";
            this.uxTitle.Size = new System.Drawing.Size(774, 92);
            this.uxTitle.TabIndex = 0;
            this.uxTitle.Text = "Title Text";
            this.uxTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uxTitleImage
            // 
            this.uxTitleImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxTitleImage.Image = ((System.Drawing.Image)(resources.GetObject("uxTitleImage.Image")));
            this.uxTitleImage.InitialImage = global::RHL.Properties.Resources.menu_logo_new;
            this.uxTitleImage.Location = new System.Drawing.Point(113, 2);
            this.uxTitleImage.Margin = new System.Windows.Forms.Padding(15, 2, 3, 2);
            this.uxTitleImage.Name = "uxTitleImage";
            this.uxTitleImage.Size = new System.Drawing.Size(308, 88);
            this.uxTitleImage.TabIndex = 1;
            this.uxTitleImage.TabStop = false;
            // 
            // uxTiltleBar_Action
            // 
            this.uxTiltleBar_Action.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.uxTiltleBar_Action.AutoSize = true;
            this.uxTiltleBar_Action.BackColor = System.Drawing.Color.Transparent;
            this.uxTiltleBar_Action.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uxTiltleBar_Action.FlatAppearance.BorderSize = 0;
            this.uxTiltleBar_Action.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxTiltleBar_Action.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxTiltleBar_Action.ForeColor = System.Drawing.Color.White;
            this.uxTiltleBar_Action.Location = new System.Drawing.Point(0, 1);
            this.uxTiltleBar_Action.Margin = new System.Windows.Forms.Padding(0);
            this.uxTiltleBar_Action.Name = "uxTiltleBar_Action";
            this.uxTiltleBar_Action.Size = new System.Drawing.Size(98, 89);
            this.uxTiltleBar_Action.TabIndex = 2;
            this.uxTiltleBar_Action.Text = "<";
            this.uxTiltleBar_Action.UseVisualStyleBackColor = false;
            this.uxTiltleBar_Action.Visible = false;
            this.uxTiltleBar_Action.Click += new System.EventHandler(this.uxTiltleBar_Action_Click);
            // 
            // uxLogoWho
            // 
            this.uxLogoWho.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.uxLogoWho.Image = global::RHL.Properties.Resources.WHO_logo_01__1_;
            this.uxLogoWho.Location = new System.Drawing.Point(1382, 3);
            this.uxLogoWho.Margin = new System.Windows.Forms.Padding(25, 3, 3, 3);
            this.uxLogoWho.Name = "uxLogoWho";
            this.uxLogoWho.Size = new System.Drawing.Size(212, 86);
            this.uxLogoWho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.uxLogoWho.TabIndex = 6;
            this.uxLogoWho.TabStop = false;
            // 
            // Title_Bar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.MediumBlue;
            this.Controls.Add(this.uxPanelTitle);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "Title_Bar";
            this.Size = new System.Drawing.Size(2511, 100);
            this.uxPanelTitle.ResumeLayout(false);
            this.uxPanelTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxTitleImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uxLogoWho)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel uxPanelTitle;
        private System.Windows.Forms.Label uxTitle;
        private System.Windows.Forms.PictureBox uxTitleImage;
        private System.Windows.Forms.Button uxTiltleBar_Action;
        private System.Windows.Forms.ToolTip uxTitlebar_Action_Tooltip;
        private System.Windows.Forms.PictureBox uxLogoWho;
    }
}
