﻿namespace RHL.UserControls
{
    partial class Term_Teaser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxTermLayout = new System.Windows.Forms.TableLayoutPanel();
            this.uxName = new System.Windows.Forms.Label();
            this.uxImage = new System.Windows.Forms.PictureBox();
            this.uxTermLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // uxTermLayout
            // 
            this.uxTermLayout.AutoSize = true;
            this.uxTermLayout.ColumnCount = 2;
            this.uxTermLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxTermLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.uxTermLayout.Controls.Add(this.uxName, 1, 0);
            this.uxTermLayout.Controls.Add(this.uxImage, 0, 0);
            this.uxTermLayout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uxTermLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxTermLayout.Location = new System.Drawing.Point(0, 0);
            this.uxTermLayout.Margin = new System.Windows.Forms.Padding(0);
            this.uxTermLayout.Name = "uxTermLayout";
            this.uxTermLayout.RowCount = 1;
            this.uxTermLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.uxTermLayout.Size = new System.Drawing.Size(231, 150);
            this.uxTermLayout.TabIndex = 0;
            this.uxTermLayout.Click += new System.EventHandler(this.Term_Teaser_Click);
            // 
            // uxName
            // 
            this.uxName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.uxName.AutoSize = true;
            this.uxName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uxName.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxName.ForeColor = System.Drawing.Color.Black;
            this.uxName.Location = new System.Drawing.Point(110, 37);
            this.uxName.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.uxName.Name = "uxName";
            this.uxName.Padding = new System.Windows.Forms.Padding(10, 20, 10, 20);
            this.uxName.Size = new System.Drawing.Size(118, 76);
            this.uxName.TabIndex = 0;
            this.uxName.Text = "label1";
            this.uxName.Click += new System.EventHandler(this.Term_Teaser_Click);
            // 
            // uxImage
            // 
            this.uxImage.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.uxImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uxImage.Location = new System.Drawing.Point(0, 39);
            this.uxImage.Margin = new System.Windows.Forms.Padding(0);
            this.uxImage.Name = "uxImage";
            this.uxImage.Size = new System.Drawing.Size(100, 71);
            this.uxImage.TabIndex = 1;
            this.uxImage.TabStop = false;
            this.uxImage.Visible = false;
            this.uxImage.Click += new System.EventHandler(this.Term_Teaser_Click);
            // 
            // Term_Teaser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.uxTermLayout);
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.Name = "Term_Teaser";
            this.Size = new System.Drawing.Size(231, 150);
            this.Load += new System.EventHandler(this.Term_Teaser_Load);
            this.uxTermLayout.ResumeLayout(false);
            this.uxTermLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel uxTermLayout;
        private System.Windows.Forms.Label uxName;
        private System.Windows.Forms.PictureBox uxImage;
    }
}
