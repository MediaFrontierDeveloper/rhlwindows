﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RHL.lib.Media;
using RHL.lib.Session;
using RHL.lib.Schema;
using RHL.lib.Db;

namespace RHL.UserControls
{
    public partial class Title_Bar : UserControl
    {
        DbManager db = DbManager.Instance;
        SessionManager ss = SessionManager.Instance;

        public string Title
        {
            get
            {
                return uxTitle.Text;
            }
            set
            {
                uxTitle.Text = value;
            }
        }

        public Title_Bar()
        {
            InitializeComponent();
            this.BackColor = ColorManager.BrandColor;
            uxTiltleBar_Action.FlatAppearance.MouseOverBackColor = ColorManager.BrandColor;
            uxTitlebar_Action_Tooltip.SetToolTip(uxTiltleBar_Action, "Go Back");
        }

        public bool ActionVisible
        {
            get
            {
                return uxTiltleBar_Action.Visible;
            }
            set
            {
                uxTiltleBar_Action.Visible = value;
            }
        }

        public void ClearBar()
        {
            uxTitle.Text = "";
        }

        private void uxTitle_Search_Click(object sender, EventArgs e)
        {
            RHLApp2 frm = (RHLApp2)this.Parent;
            frm.SetFormState(RHLApp2.FormStates.Search);
        }

        private void uxTitle_About_Click(object sender, EventArgs e)
        {
            RHLApp2 frm = (RHLApp2)this.Parent;
            frm.SetFormState(RHLApp2.FormStates.About);
        }

        private void uxTiltleBar_Action_Click(object sender, EventArgs e)
        {
            RHLApp2 frm = (RHLApp2)this.Parent;
            if(ss.PreviousTerm > 0)
            {
                Term t = db.GetTerm(Properties.Settings.Default.Language, ss.PreviousTerm);
                frm.LoadTermDetail(t);
            }
            else
            {
                frm.LoadTerms();
                frm.LoadNewInRhl();
                frm.SetFormState(RHLApp2.FormStates.Default);
            }
            
        }
    }
}
