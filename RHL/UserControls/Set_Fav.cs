﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RHL.lib.Db;
using RHL.lib.Media;

namespace RHL.UserControls
{
    public partial class Set_Fav : UserControl
    {
        private DbManager db;
        public string Type { get; set; }
        public int Nid { get; set; }
        public bool IsFav { get; private set; }
        private ToolTip tt;

        public Set_Fav()
        {
            InitializeComponent();
            db = DbManager.Instance;
            Type = string.Empty;
            Nid = 0;
            tt = new ToolTip();
        }

        public void LoadFav(string type, int nid)
        {
            Type = type;
            Nid = nid;
            IsFav = db.IsFav(type, nid);
            if(IsFav)
            {
                uxImage.Image = Assets.GetImage("icon-favourite-highlighted3x.png", 66, 66);
                tt.SetToolTip(uxImage, "Click to unfavourite this article.");
            }
            else
            {
                uxImage.Image = Assets.GetImage("icon-favourite3x.png", 66, 66);
                tt.SetToolTip(uxImage, "Click to favourite this article.");
            }
        }

        private void uxImage_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Type) && Nid > 0)
            {
                db.AddOrDeleteFav(Type, Nid);
                LoadFav(Type, Nid);
                RHLApp2 rhlApp = (RHLApp2)Application.OpenForms["RHLApp2"];
                if (rhlApp != null)
                {
                    TabControl tab = (TabControl)rhlApp.Controls["uxTabs"];
                    if (tab != null)
                    {
                        if (tab.SelectedTab.Tag != null)
                        {
                            if (tab.SelectedTab.Tag.ToString() == "favs")
                                rhlApp.LoadFavs();
                        }
                    }
                    rhlApp.UpdateFavsCount(-1);
                }
            }
        }
    }
}
