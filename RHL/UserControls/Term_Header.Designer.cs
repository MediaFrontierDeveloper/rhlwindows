﻿namespace RHL.UserControls
{
    partial class Term_Header
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxDescription = new System.Windows.Forms.Label();
            this.uxImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.uxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // uxDescription
            // 
            this.uxDescription.AutoEllipsis = true;
            this.uxDescription.BackColor = System.Drawing.Color.LightGray;
            this.uxDescription.Font = new System.Drawing.Font("Arial", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxDescription.ForeColor = System.Drawing.Color.Black;
            this.uxDescription.Location = new System.Drawing.Point(356, 1);
            this.uxDescription.Name = "uxDescription";
            this.uxDescription.Size = new System.Drawing.Size(753, 206);
            this.uxDescription.TabIndex = 0;
            this.uxDescription.Text = "Description";
            this.uxDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uxImage
            // 
            this.uxImage.Location = new System.Drawing.Point(21, 1);
            this.uxImage.Name = "uxImage";
            this.uxImage.Size = new System.Drawing.Size(329, 206);
            this.uxImage.TabIndex = 1;
            this.uxImage.TabStop = false;
            // 
            // Term_Header
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.uxImage);
            this.Controls.Add(this.uxDescription);
            this.Name = "Term_Header";
            this.Size = new System.Drawing.Size(1507, 210);
            ((System.ComponentModel.ISupportInitialize)(this.uxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label uxDescription;
        private System.Windows.Forms.PictureBox uxImage;
    }
}
