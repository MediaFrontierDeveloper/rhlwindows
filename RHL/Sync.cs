﻿using System;
using System.IO;
using System.Windows.Forms;
using RHL.lib.Db;
using RHL.lib.Media;
using RHL.lib.Sync;
using RHL.lib.Messages;
using RHL.lib.Session;
using System.Collections.Generic;
using System.Linq;

namespace RHL
{
    internal class Sync
    {

        private DbManager _db;
        private SyncManager _sd;
        private SessionManager _sm;

        public Sync()
        {
            _db = DbManager.Instance;
            _sd = SyncManager.Instance;
            _sm = SessionManager.Instance;
            _sm.HasResynced = false;
        }

        internal EventHandler SyncDone;

        internal void OnSyncDone(EventArgs e)
        {
            if (SyncDone != null)
                SyncDone(new object(), e);
        }

        internal void UpdateFormLabel(string text)
        {
            AppMessage.Get.MessageText = text;
        }

        internal void DoSync(bool doSync = true)
        {
            try
            {
                if(!doSync)
                    return;
                UpdateFormLabel("Starting Sync...");
                // copy the db if running for the first time
                // Check its there first!
                if (!File.Exists(_db.DbPath))
                {
                    UpdateFormLabel("Copying inital data");
                    Stream file = Assets.ResourceFile(_db.DbName);
                    using (FileStream output = File.Create(_db.DbPath))
                    {
                        file.CopyTo(output);
                    }
                }

                // Now check to see if we are online or not 
                // If not, we go into offline mode
                UpdateFormLabel("Checking connection");
                if(!_sd.HasConnection())
                {
                    UpdateFormLabel("No connection: working in offline mode");
                    _sm.IsOnline = false;
                    return;
                }

                // All good, we must be online!
                _sm.IsOnline = true;

                int sync_time = Queries.UnixTimeStamp;
                List<string> langs = new List<string>
                {
                    { "en" },
                    { "fr" },
                    { "es"  },
                    { "zh-hans" },
                    { "pt-br" },
                    //{ "ru" },
                };
                Dictionary<string, string> langDict = new Dictionary<string, string>
                {
                    { "en", "English" },
                    { "fr", "French" },
                    { "es", "Spanish" },
                    { "zh-hans", "Chinese" },
                    { "pt-br", "Portuguese" },
                    //{ "ru", "Russian" },
                };
                
                langs.ForEach(l => { UpdateFormLabel(string.Format("Syncing: {0} categories", langDict[l])); _sd.SyncTerms(l); });
                langs.ForEach(l => { UpdateFormLabel(string.Format("Syncing: {0} articles", langDict[l]));  _sd.SyncArticles(l); });
                langs.ForEach(l => { UpdateFormLabel(string.Format("Syncing: {0} videos", langDict[l])); _sd.SyncVideos(l); });
                langs.ForEach(l => { UpdateFormLabel(string.Format("Syncing: {0} guidelines", langDict[l])); _sd.SyncGuidelines(l); });

                // Clear the new table
                _sd.ClearNew();
                langs.ForEach(l => { UpdateFormLabel(string.Format("Syncing {0} New in RHL", langDict[l])); _sd.SyncNew(l); });
                _db.UpdateSync(sync_time);
            }
            catch
            {
                //throw;
                // TODO: remove this before release!!!!
            }
            finally
            {
                // Even if the sync fails go to the main app
                _db = null;
                _sd = null;
                OnSyncDone(EventArgs.Empty);
            }
        }

    }
}
