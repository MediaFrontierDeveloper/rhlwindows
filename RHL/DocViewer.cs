﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RHL
{
    public partial class DocViewer : Form
    {
        public string DocUrl { get; set; }
        public DocViewer()
        {
            InitializeComponent();
        }
        public void LoadDoc(string docUrl)
        {
            DocUrl = docUrl;
            uxBrowser.Navigate(DocUrl, false);
            //uxBrowser.DocumentText = string.Format(DocHtml(), DocUrl);
        }

        public string DocHtml()
        {
            string docHtml = @"<body>
            <object classid=""clsid:ca8a9780-280d-11cf-a24d-444553540000"" id=""pdf1"" type=""application/pdf"" data=""file:///{0}"" style=""width: 100%; height: 100%"" >
<param name=""src"" value=""file:///{0}""></param>
   </object>
 </body>";
            return docHtml;
        }
    }
}
